import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.stats as sps
from matplotlib import cm
import sys

from scipy.interpolate import interp1d

from collections import OrderedDict

mpl.rcParams['font.size']=18
# mpl.rcParams['figure.figsize']=(8,6)
mpl.rcParams['lines.linewidth']=2


# Load the mesoscopic data
df_meso = pd.read_csv("gathered-channels-meso.csv",index_col=0)
df_meso['dcap'] = df_meso.LC/4.0
LP = 10.
LC = df_meso.LC
df_meso['RG'] = ((1./3.)*LP*LC - LP**2 +
                 (2.*LP**3/LC)*( 1-(LP/LC)*(1-np.exp(-LC/LP))))**0.5
df_meso = df_meso[np.abs(df_meso.Zchannel-75)>3] # Remove N=75 for plotting purposes
#df_meso = df_meso[np.abs(df_meso.Rchannel-30)>3]
#df_meso = df_meso[np.abs(df_meso.Rchannel-50)>3]
#df_meso = df_meso[df_meso.Rchannel>df_meso.dcap]
#df_meso = df_meso[df_meso.Zchannel>df_meso.dcap]
df_meso = df_meso[df_meso.Rchannel>df_meso.dcap+df_meso.RG]
df_meso = df_meso[df_meso.Zchannel>df_meso.dcap+df_meso.RG]
# df_meso = df_meso[df_meso.Rchannel>3*(df_meso.dcap+df_meso.RG)]
# df_meso = df_meso[df_meso.Zchannel>3*(df_meso.dcap+df_meso.RG)]
# df_meso = df_meso[df_meso.Rchannel>50]
# df_meso = df_meso[df_meso.Zchannel>99]
print("Mesoscopic data loaded.")
Rchs = np.sort(np.unique(df_meso['Rchannel']))
Zchs = np.sort(np.unique(df_meso['Zchannel']))
tname='tmeso'
#df_igmeso = pd.read_csv("plots/%s_IGfits.csv"%(tname),index_col=0)

df_meso = df_meso[~((np.abs(df_meso.Zchannel-500)<1)&(np.abs(df_meso.Rchannel-1000)<1)&(np.abs(df_meso.LC-10<1)))]

### Main function at the end of file


# Load microscopic data
df_micro = pd.read_csv("../micro/gathered-channels-micro.csv",index_col=0)


###########################################################################################

def Meso_GroupPlot(df):

    # fig, axarr = plt.subplots(3,1,dpi=300,figsize=(3.4,7.2))
    fig, axarr = plt.subplots(3,1,dpi=700,figsize=(8,17))
    cmap = plt.get_cmap('Dark2')
    df.sort_values(by='LC',inplace=True)
    df_micro.sort_values(by='N',inplace=True)

    
    # Plot mean tmeso
    plt.sca(axarr[0])
    for key, grp in df.groupby(['Rchannel','Zchannel']):
        plt.semilogy(grp['LC'].unique(),
                     grp.groupby('LC')['tmeso'].mean(),'o-',
                     label='%d'%key[1],
                     c=cmap(np.argmin(np.abs(key[1]-Zchs))/float(len(Zchs))))
    # Legend
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(),title=r'$Z_\mathrm{ch}$',fontsize=14)
    # Plot tmicro
    # df_micro.groupby('N').mean()['t'].plot(style='ks--')
    plt.plot(df_micro['N'].unique(),
             df_micro.groupby('N').mean()['t'],'ks--')
    plt.text(50,200,'Microscopic Time')
    plt.xlabel('Chain Length, $N$')
    plt.ylabel('Mean $t_\mathrm{meso}$')
    plt.xlim([0,310])
    plt.text(250,7e6,'(a)',fontsize=20)

    
    # Plot mean tmeso/volume against N
    plt.sca(axarr[1])
    for key, grp in df.groupby(['Rchannel','Zchannel']):
        Volume = np.pi*(key[0]**2*key[1] - 0.5*(4./3.)*grp.groupby('LC')['dcap'].mean()**3)
        plt.plot(grp['LC'].unique(),
                 grp.groupby('LC')['tmeso'].mean()/Volume,'o-',
                 label='%d'%key[1],
                 c=cmap(np.argmin(np.abs(key[1]-Zchs))/float(len(Zchs))))
        plt.xlabel('Chain Length, $N$')
        plt.ylabel('Mean $t_\mathrm{meso}$ / Volume')
    # Legend
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(),title=r'$Z_\mathrm{ch}$',fontsize=14)
    plt.xlim([0,310])
    plt.text(250,0.063,'(b)',fontsize=20)


    # Plot mean tmeso/volume against normalized chain length -- universal curve!
    plt.sca(axarr[2])
    for key, grp in df.groupby(['Rchannel','Zchannel']):
        Volume = np.pi*(key[0]**2*key[1] - 0.5*(4./3.)*grp.groupby('LC')['dcap'].mean()**3)
        plt.plot(grp['LC'].unique()/(key[1]/3.5716),
                 grp.groupby('LC')['tmeso'].mean()/Volume,'o-',
                 label='%d'%key[1],
                 c=cmap(np.argmin(np.abs(key[1]-Zchs))/float(len(Zchs))))
    plt.xlim([-1.0,10.0])
    plt.xlabel('Normalized Chain Length, $N / N^*$')
    plt.ylabel('Mean $t_\mathrm{meso}$ / Volume')
    plt.axvline(1.0,linestyle='--',c='k')
    plt.text(1.25,0.0675,'$N=N^*$')
    plt.axvline(3.5716*4,linestyle='-',c='k')
    plt.text(-0.95,0.045,'Diffusive',rotation=-30)
    plt.text(1.5,0.03,'Driven',rotation=-30)
    # Legend
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(),title=r'$Z_\mathrm{ch}$',fontsize=14)
    plt.text(6,0.06,'(c)',fontsize=20)

    
    # Save
    plt.tight_layout()
    plt.savefig("plots/meso-results.eps")
    plt.close()




def IGAnalyse(df):

    # Recall: mymu = L/v, mylambda = L^2/2D
    df['mymu_fit'] = df['pymu_fit']*df['pylambda_fit']
    df['mylambda_fit'] = df['pylambda_fit']
    df['v_over_L'] = 1./df['mymu_fit']
    df['twoD_over_L2'] = 1./df['mylambda_fit']
    df['twoD_over_v2'] = df['twoD_over_L2'] / df['v_over_L']**2.
    df['myvar'] = df['mymu_fit']**3./df['mylambda_fit']
    df['mystd'] = df['myvar']**0.5
    alpha_pylambda = 1.
    alpha_mylambda = 1.
    alpha_mymu = -1.

def Meso_BasicPlots(df):


    # Plot mean tmeso
    df.sort_values(by='LC',inplace=True)
    plt.figure(dpi=300)
    cmap = plt.get_cmap('Dark2')
    for key, grp in df.groupby(['Rchannel','Zchannel']):
        plt.semilogy(grp['LC'].unique(),
                     grp.groupby('LC')['tmeso'].mean(),'o-',
                     label='%.1f'%key[1],
                     c=cmap(np.argmin(np.abs(key[1]-Zchs))/float(len(Zchs))),
                     lw=2)
    # Legend outside the axes
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(),title=r'$Z_\mathrm{ch}$',fontsize=12)
    # Plot tmicro
    df_micro.groupby('N').mean()['t'].plot(style='ks--')
    plt.text(50,200,'Microscopic Time')
    plt.xlabel('Chain Length, $N$')
    plt.ylabel('Mean $t_\mathrm{meso}$')
    plt.xlim([0,310])
    # Save
    plt.tight_layout()
    plt.savefig("plots/meso-mean.eps",dpi=300)
    plt.close()

    # Plot mean tmeso/volume against N
    df.sort_values(by='LC',inplace=True)
    plt.figure(dpi=300)
    cmap = plt.get_cmap('Dark2')
    for key, grp in df.groupby(['Rchannel','Zchannel']):
        Volume = np.pi*(key[0]**2*key[1] - 0.5*(4./3.)*grp.groupby('LC')['dcap'].mean()**3)
        plt.plot(grp['LC'].unique(),
                 grp.groupby('LC')['tmeso'].mean()/Volume,'o-',
                 label='%.1f'%key[1],
                 c=cmap(np.argmin(np.abs(key[1]-Zchs))/float(len(Zchs))),
                 lw=2)
        plt.xlabel('Chain Length, $N$')
        plt.ylabel('Mean $t_\mathrm{meso}$ / Volume')
    # Legend outside the axes
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(),title=r'$Z_\mathrm{ch}$',fontsize=12)
    plt.xlim([0,310])
    # Save
    plt.tight_layout()
    plt.savefig("plots/meso-mean-by-V.eps",dpi=300)
    plt.close()


    # Plot mean tmeso/volume against normalized chain length -- universal curve!
    df.sort_values(by='LC',inplace=True)
    plt.figure(dpi=300)
    cmap = plt.get_cmap('Dark2')
    for key, grp in df.groupby(['Rchannel','Zchannel']):
        Volume = np.pi*(key[0]**2*key[1] - 0.5*(4./3.)*grp.groupby('LC')['dcap'].mean()**3)
        plt.plot(grp['LC'].unique()/(key[1]/3.5716),
                 grp.groupby('LC')['tmeso'].mean()/Volume,'o-',
                 label='%.1f'%key[1],
                 c=cmap(np.argmin(np.abs(key[1]-Zchs))/float(len(Zchs))),
                 lw=2)
        # plt.plot(grp['LC'].unique()/(4*key[1]),
        #          grp.groupby('LC')['tmeso'].mean()/Volume,'o-',
        #          label='%.1f'%key[1],
        #          c=cmap(np.argmin(np.abs(key[1]-Zchs))/float(len(Zchs))),
        #          lw=2)
    # plt.xlabel('$N / N_0$, where $r_\mathrm{cap}(N_0) = Z_\mathrm{ch}$')
    plt.xlim([-1.0,10.0])
    plt.xlabel('$N / N^*$')
    plt.ylabel('Mean $t_\mathrm{meso}$ / Volume')
    plt.axvline(1.0,linestyle='--',c='k')
    plt.text(1.25,0.0675,'$N=N^*$')
    plt.axvline(3.5716*4,linestyle='-',c='k')
    plt.text(14.25,0.0675,'$N=N_\mathrm{max}$')
    plt.text(-0.95,0.045,'Diffusive',rotation=-30)
    plt.text(1.5,0.03,'Driven',rotation=-30)
    # Legend outside the axes
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(),title=r'$Z_\mathrm{ch}$',fontsize=12)
    # # Fit
    # LCnorms = np.linspace(0,0.6)
    # heightfit = 0.065
    # sigfit = 0.25
    # plt.plot(LCnorms, heightfit * np.exp(-0.5*LCnorms**2/sigfit**2),'k--')
    # plt.text(0.05,0.01,r'Fit: $%.3f e^{-\frac{(N/N_0)^2}{2(%.2f)^2}}$'%(heightfit,sigfit),fontsize=16)
    # Save
    plt.tight_layout()
    plt.savefig("plots/meso-mean-universal-fit.eps",dpi=300)
    plt.close()


    
    # # Log-log Plot mean tmeso/volume against normalized chain length -- universal curve!
    # df.sort_values(by='LC',inplace=True)
    # plt.figure(dpi=300)
    # cmap = plt.get_cmap('Dark2')
    # for key, grp in df.groupby(['Rchannel','Zchannel']):
    #     Volume = np.pi*(key[0]**2*key[1] - 0.5*(4./3.)*grp.groupby('LC')['dcap'].mean()**3)
    #     plt.loglog(grp['LC'].unique()/(key[1]/3.5716),
    #              grp.groupby('LC')['tmeso'].mean()/Volume,'o-',
    #              label='%.1f'%key[1],
    #              c=cmap(np.argmin(np.abs(key[1]-Zchs))/float(len(Zchs))),
    #              lw=2)
    # plt.xlabel('$N / N^*$')
    # plt.ylabel('Mean $t_\mathrm{meso}$ / Volume')
    # plt.ylim(ymax=0.101)
    # plt.axvline(1.0,linestyle='--',c='k')
    # plt.text(1.15,0.075,'$N=N^*$')
    # plt.axvline(3.5716*4,linestyle='-',c='k')
    # plt.text(4.25,0.075,'$N=N_\mathrm{max}$')
    # plt.text(0.2,0.01,'Diffusive')
    # plt.text(1.5,0.01,'Driven')
    # # Legend outside the axes
    # handles, labels = plt.gca().get_legend_handles_labels()
    # by_label = OrderedDict(zip(labels, handles))
    # plt.legend(by_label.values(), by_label.keys(),title=r'$Z_\mathrm{ch}$',fontsize=12)
    # # Save
    # plt.tight_layout()
    # plt.savefig("plots/meso-mean-universal-fit-loglog.eps",dpi=300)
    # plt.close()


    # # Plot mean tmeso/volume against N and average over Rch and add theoretical threshold N values
    # df.sort_values(by='LC',inplace=True)
    # plt.figure(dpi=300)
    # cmap = plt.get_cmap('Dark2')
    # df['Volume'] = np.pi*(df['Rchannel']**2*df['Zchannel'] - 0.5*(4./3.)*df['dcap']**3)
    # df['tmeso/Volume'] = df['tmeso']/df['Volume']
    # for Zch, grp in df.groupby('Zchannel'):
    #     curxdata = grp['LC'].unique()
    #     curydata = grp.groupby('LC')['tmeso/Volume'].mean()
    #     f = interp1d(curxdata,curydata)
    #     plt.plot(curxdata,
    #              curydata,'o-',
    #              label='%.1f'%Zch,
    #              c=cmap(np.argmin(np.abs(Zch-Zchs))/float(len(Zchs))),
    #              lw=2)
    #     Nstar = Zch/3.5716
    #     if Nstar<min(curxdata):
    #         plt.plot(Nstar,curydata.iloc[0],'*',
    #                  c=cmap(np.argmin(np.abs(Zch-Zchs))/float(len(Zchs))),
    #                  ms=12,
    #                  markeredgecolor='k')
    #     else:
    #         plt.plot(Nstar,f(Nstar),'*',
    #                  c=cmap(np.argmin(np.abs(Zch-Zchs))/float(len(Zchs))),
    #                  ms=12,
    #                  markeredgecolor='k')
    #     plt.xlabel('Chain Length, $N$')
    #     plt.ylabel('Mean $t_\mathrm{meso}$ / Volume, Averaged over $R_\mathrm{ch}$')
    # # Legend outside the axes
    # handles, labels = plt.gca().get_legend_handles_labels()
    # by_label = OrderedDict(zip(labels, handles))
    # plt.legend(by_label.values(), by_label.keys(),title=r'$Z_\mathrm{ch}$',fontsize=12)
    # # Save
    # plt.tight_layout()
    # plt.savefig("plots/meso-mean-by-V_avgRch.eps",dpi=300)
    # plt.close()
    
    # # Plot std tmeso
    # df.sort_values(by='LC',inplace=True)
    # plt.figure(dpi=300)
    # cmap = plt.get_cmap('Dark2')
    # for key, grp in df.groupby(['Rchannel','Zchannel']):
    #     plt.plot(grp['LC'].unique(),
    #              grp.groupby('LC')['tmeso'].std(),'o-',
    #              label='%.1f'%key[1],
    #              c=cmap(np.argmin(np.abs(key[1]-Zchs))/float(len(Zchs))),
    #              lw=2)
    #     plt.xlabel('Chain Length, $N$')
    #     plt.ylabel('Standard Deviation of $t_\mathrm{meso}$')
    # # Legend outside the axes
    # handles, labels = plt.gca().get_legend_handles_labels()
    # by_label = OrderedDict(zip(labels, handles))
    # plt.legend(by_label.values(), by_label.keys(),title=r'$Z_\mathrm{ch}$',fontsize=12)
    # # Save
    # plt.yscale('log')
    # plt.tight_layout()
    # plt.savefig("plots/meso-std.eps")
    # plt.close()

    # # Plot std tmeso/volume (incl. dcap)
    # df.sort_values(by='LC',inplace=True)
    # plt.figure(dpi=300)
    # cmap = plt.get_cmap('Dark2')
    # for key, grp in df.groupby(['Rchannel','Zchannel']):
    #     Volume = np.pi*(key[0]**2*key[1] - 0.5*(4./3.)*grp.groupby('LC')['dcap'].mean()**3)
    #     plt.plot(grp['LC'].unique()/(key[1]/3.5716),
    #              grp.groupby('LC')['tmeso'].std()/Volume,'o-',
    #              label='%.1f'%key[1],
    #              c=cmap(np.argmin(np.abs(key[1]-Zchs))/float(len(Zchs))),
    #              lw=2)
    #     # plt.plot(grp['LC'].unique()/(4*key[1]),
    #     #          grp.groupby('LC')['tmeso'].std()/Volume,'o-',
    #     #          label='%.1f'%key[1],
    #     #          c=cmap(np.argmin(np.abs(key[1]-Zchs))/float(len(Zchs))),
    #     #          lw=2)
    #     plt.xlabel('$N / N^*$')
    #     # plt.xlabel('$N / N_0$, where $r_\mathrm{cap}(N_0) = Z_\mathrm{ch}$')
    #     plt.ylabel('Standard Deviation of $t_\mathrm{meso}$ / Volume')
    # # Legend outside the axes
    # handles, labels = plt.gca().get_legend_handles_labels()
    # by_label = OrderedDict(zip(labels, handles))
    # plt.legend(by_label.values(), by_label.keys(),title=r'$Z_\mathrm{ch}$',fontsize=12)
    # # Save
    # plt.tight_layout()
    # plt.savefig("plots/meso-std-by-V.eps")
    # plt.close()


   
    

#######################################################################################




# Meso_BasicPlots(df_meso)

Meso_GroupPlot(df_meso)



















#######################################################################################
#######################################################################################
#######################################################################################
#######################################################################################

     
# def Meso_AdvPlots(df):

#     #
        
#     #
#     plt.figure(dpi=300)
#     Rch0 = df.Rchannel.unique()[0]
#     for key, grp in df.groupby(['Rchannel','Zchannel']):
#         if tname=='tmeso':
#             dcap = grp['LC']/4.0
#         if tname=='tcross':
#             dcap = grp['LC']/4.0 - key[1]/8.0
#         if key[0]==Rch0:
#             plt.loglog(grp['LC'],((key[1]-dcap)/grp['realmean']),color=cm.gnuplot(1.0-key[1]/df.Zchannel.max()),label='Zch = %.1f'%key[1])
#         else:
#             plt.loglog(grp['LC'],((key[1]-dcap)/grp['realmean']),color=cm.gnuplot(1.0-key[1]/df.Zchannel.max()))
#     plt.legend(fontsize=12)
#     plt.xlabel('N')
#     plt.ylabel('(Zch-dcap)/Time')
#     plt.title('Coloured by Zch')
#     # Save
#     plt.tight_layout()
#     plt.savefig("plots/%s-speed.eps"%tname)
#     plt.close()
    
#     # #
#     # plt.figure(dpi=300)
#     # Rch0 = df.Rchannel.unique()[0]
#     # for key, grp in df.groupby(['Rchannel','Zchannel']):
#     #     if tname=='tmeso':
#     #         dcap = grp['LC']/4.0
#     #     if tname=='tcross':
#     #         dcap = grp['LC']/4.0 - key[1]/8.0
#     #     if key[0]==Rch0:
#     #         plt.loglog(grp['LC'],((key[1]-dcap)/grp['realmean'])*key[0]**2.0,color=cm.gnuplot(1.0-key[1]/df.Zchannel.max()),label='Zch = %.1f'%key[1])
#     #     else:
#     #         plt.loglog(grp['LC'],((key[1]-dcap)/grp['realmean'])*key[0]**2.0,color=cm.gnuplot(1.0-key[1]/df.Zchannel.max()),label=None)
#     # plt.legend(fontsize=12)
#     # plt.xlabel('N')
#     # plt.ylabel('(Zch-dcap/Time)*Rch^2')
#     # plt.title('Coloured by Zch')
#     # # Save
#     # plt.tight_layout()
#     # plt.savefig("plots/%s-speed-normed.eps"%tname)
#     # plt.close()

#     # # Assuming D=1/N, v~N, estimate L
#     # dt = 0.01 # Can get units right on L estimate
#     # df['Lest_v'] = df['mymu_fit'] * df.LC * dt
#     # df['Dest'] = 1./df.LC
#     # df['Lest_D'] = (df['mylambda_fit'] * dt * 2.*df.Dest )**0.5

#     # # Assuming v = F * LC, and Fch relates to Fpore by cons. flux
#     # Rpore = 0.8
#     # Zpore = 1.0
#     # V0 = 17.312367 # From 'gain' variable in meso run log 
#     # df['Fchannel'] = V0/(df.Zchannel + Zpore*(df.Rchannel/Rpore)**2.)
#     # df['vest'] = df.Fchannel * df.LC
#     # df['Lest_vest'] = df['mymu_fit'] * dt * df.vest 

#     # # Plot L estimate from v
#     # plt.figure(dpi=300)
#     # for key, grp in df.groupby(['Rchannel','Zchannel']):
#     #     plt.loglog(grp['LC'], grp['Lest_v'],label='Rch = %.1f, Zch = %.1f'%key)
#     #     plt.xlabel('N')
#     #     plt.ylabel(r'$L$ Estimated from $v\propto N$')
#     # plt.legend()
#     # #plt.show()
#     # plt.savefig("plots/%s-Lestv.eps"%tname)
#     # plt.close()

#     # # Plot L estimate from D
#     # plt.figure(dpi=300)
#     # for key, grp in df.groupby(['Rchannel','Zchannel']):
#     #     plt.loglog(grp['LC'], grp['Lest_D'],label='Rch = %.1f, Zch = %.1f'%key)
#     #     plt.xlabel('N')
#     #     plt.ylabel(r'$L$ Estimated from $D = 1/N$')

#     #     # Plot relevant length scales
#     #     Zcross = 0.875*key[1]
#     #     Zcap = 0.125*key[1]
#     #     if (tname=='tmeso'):
#     #         plt.axhline(key[1],color='k',linestyle='--') 
#     #     if (tname=='tcross'):
#     #         plt.axhline(Zcross,color='k',linestyle='--') # Line at actual Zcross
#     #     if (tname=='tcap'):
#     #         # No capture radius
#     #         plt.axhline(Zcap,color='k',linestyle='--')
#     #         # Purely in line - smallest length scale
#     #         myLCs = np.linspace(10,200,1e2)
#     #         dcap = 2*myLCs**0.5
#     #         plt.plot(myLCs,Zcap-dcap,color='k',linestyle='-')

#     # plt.legend()
#     # #plt.show()
#     # plt.savefig("plots/%s-LestD.eps"%tname)
#     # plt.close()

#     # # Plot vest
#     # plt.figure(dpi=300)
#     # for key, grp in df.groupby(['Rchannel','Zchannel']):
#     #     plt.loglog(grp['LC'], grp['vest'],label='Rch = %.1f, Zch = %.1f'%key)
#     #     plt.xlabel('N')
#     #     plt.ylabel(r'Estimated $v$')
#     # plt.legend()
#     # #plt.show()
#     # plt.savefig("plots/%s-vest.eps"%tname)
#     # plt.close()

#     # # Using estimated v, estimate L
#     # plt.figure(dpi=300)
#     # for key, grp in df.groupby(['Rchannel','Zchannel']):
#     #     plt.loglog(grp['LC'], grp['Lest_vest'],label='Rch = %.1f, Zch = %.1f'%key)
#     #     plt.xlabel('N')
#     #     plt.ylabel(r'$L$ Estimated from $v$ estimate')
 
#     #    # Plot relevant length scales
#     #     Zcross = 0.875*key[1]
#     #     Zcap = 0.125*key[1]
#     #     if (tname=='tmeso'):
#     #         plt.axhline(key[1],color='k',linestyle='--') 
#     #     if (tname=='tcross'):
#     #         plt.axhline(Zcross,color='k',linestyle='--') # Line at actual Zcross
#     #     if (tname=='tcap'):
#     #         # No capture radius
#     #         plt.axhline(Zcap,color='k',linestyle='--')
#     #         # Purely in line - smallest length scale
#     #         myLCs = np.linspace(10,200,1e2)
#     #         dcap = 2*myLCs**0.5
#     #         plt.plot(myLCs,Zcap-dcap,color='k',linestyle='-')

#     # plt.legend()
#     # #plt.show()
#     # plt.savefig("plots/%s-Lestvest.eps"%tname)
#     # plt.close()



