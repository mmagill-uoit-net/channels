import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import sys
import glob
import scipy.stats as sps
import scipy.signal as sgnl
import scipy.special as spec

mpl.rcParams['font.size']=18
mpl.rcParams['figure.figsize']=(8,6)
mpl.rcParams['lines.linewidth']=3



# Load the microscopic data
df_micro = pd.read_csv("../micro/gathered-channels-micro.csv",index_col=0)
print("Microscopic data loaded.")

# Load the mesoscopic data
df_meso = pd.read_csv("gathered-channels-meso.csv",index_col=0)
df_meso['dcap'] = df_meso.LC/4.0
LP = 10.
LC = df_meso.LC
df_meso['RG'] = ((1./3.)*LP*LC - LP**2 +
                 (2.*LP**3/LC)*( 1-(LP/LC)*(1-np.exp(-LC/LP))))**0.5
df_meso = df_meso[df_meso.Rchannel>df_meso.dcap+df_meso.RG]
df_meso = df_meso[df_meso.Zchannel>df_meso.dcap+df_meso.RG]

df_meso = df_meso[df_meso.Rchannel<1000]
df_meso = df_meso[df_meso.Zchannel<1000]

# Plots for paper
#df_meso = df_meso[((df_meso.Rchannel==30)&(df_meso.Zchannel==90))]
#df_meso = df_meso[((df_meso.Rchannel==90)&(df_meso.Zchannel==60))]
#df_meso = df_meso[((df_meso.Rchannel==90)&(df_meso.Zchannel==75))]


#df_meso = df_meso[((df_meso.Rchannel==150)&(df_meso.Zchannel==150))]
# df_meso = df_meso[((df_meso.Rchannel==50)&(df_meso.Zchannel==500))]
#df_meso = df_meso[((df_meso.Rchannel==30)&(df_meso.Zchannel==75))]
#df_meso = df_meso[((df_meso.Rchannel==90)&(df_meso.Zchannel==60))]
#df_meso = df_meso[((df_meso.Rchannel==60)&(df_meso.Zchannel==60))]
print("Mesoscopic data loaded.")



# # CLT approximation for mobility plots
# df_meta = []
# for Rch,dfR_meso in df_meso.groupby('Rchannel'):
#     for Zch,df0_meso in dfR_meso.groupby('Zchannel'):
#         Ns = np.intersect1d(df_micro.N.unique(),df0_meso.LC.unique())
#         for N in Ns:
#             # Restrict attention to this chain length
#             dfN_micro = df_micro[df_micro.N==N]
#             dfN_meso = df0_meso[df0_meso.LC==N]
#             # Statistics
#             mu_micro = dfN_micro.t.mean()
#             var_micro = dfN_micro.t.var()
#             mu_meso = dfN_meso.tmeso.mean()
#             var_meso = dfN_meso.tmeso.var()
#             mu_macro = mu_micro+mu_meso
#             var_macro = var_micro+var_meso
#             df_meta.append([Rch,Zch,N,mu_macro,var_macro])
# df_meta = pd.DataFrame(df_meta,columns=['Rch','Zch','N','mu_macro','var_macro'])
# df_meta['sig_macro'] = df_meta['var_macro']**0.5
# df_meta['mobility'] = 1./df_meta['mu_macro'] # Long time limit of the mobility
# df_meta['cov'] = df_meta['sig_macro']/df_meta['mu_macro']

# res_rats = []
# for [Rch,Zch],grp in df_meta.groupby(['Rch','Zch']):
#     res_rat = grp.mobility.max()/grp.mobility.min()
#     plt.plot(grp.N,grp.mobility/1e-6,label='Best Ratio = %.1f'%res_rat)
#     plt.legend()
#     plt.ylim(ymin=0)
#     plt.xlabel('Chain Length')
#     plt.ylabel('Limiting Mobility / $10^{-6}$')
#     plt.tight_layout()
#     plt.savefig('plots/theomob-%.0f-%.0f.png'%(Rch,Zch))
#     plt.close()
#     res_rats.append([Rch,Zch,res_rat])
# res_rats = np.array(res_rats)
# plt.scatter(res_rats[:,0],res_rats[:,1],c=np.log(res_rats[:,2]),s=400)
# plt.colorbar()
# plt.xlabel('Rch')
# plt.ylabel('Zch')
# plt.title('Max Mobility Ratio')
# plt.tight_layout()
# plt.savefig('plots/resrats.png',dpi=200)
# plt.close()
    

# # CLT approximation for spatial distribution
# for Rch,dfR_meso in df_meso.groupby('Rchannel'):
#     for Zch,df0_meso in dfR_meso.groupby('Zchannel'):
#         Ns = np.intersect1d(df_micro.N.unique(),df0_meso.LC.unique())
#         fig, axarr = plt.subplots(2,1,figsize=(8,12))
#         # t0 = 4e7                # (30,90)
#         # t0 = 4e7                # (90,60)
#         t0 = 3e7                # (90,60)
#         kmax = 5e3
#         ks = np.arange(1,kmax)
#         # ts = np.linspace(0,t0,100)
#         ts = np.linspace(0,t0,50)
#         TT, KK = np.meshgrid(ts,ks)
#         for N in Ns:
#             # Restrict attention to this chain length
#             dfN_micro = df_micro[df_micro.N==N]
#             dfN_meso = df0_meso[df0_meso.LC==N]
#             # Statistics
#             mu_micro = dfN_micro.t.mean()
#             var_micro = dfN_micro.t.var()
#             mu_meso = dfN_meso.tmeso.mean()
#             var_meso = dfN_meso.tmeso.var()
#             Pktheo = 0.5*((TT+KK*(mu_micro+mu_meso))*
#                           np.exp(-(TT-KK*(mu_micro+mu_meso))**2/
#                                  (2*KK*(var_micro+var_meso)))/
#                           (2.0*np.pi*KK**3*(var_micro+var_meso))**0.5)
#             # plt.pcolor(TT,KK,Pktheo)
#             # plt.show()
#             # sys.exit()
#             E_ks = (KK*Pktheo).sum(axis=0) # Expected value of k over time
#             E_ksqrs = (KK**2*Pktheo).sum(axis=0) # Expected value of k^2 over time
#             Std_ks = (E_ksqrs - E_ks**2)**0.5
#             axarr[0].errorbar(ts/1e6,
#                          E_ks,
#                          yerr=Std_ks,
#                          label='N = %d'%N)
#             # axarr[1].plot(ks/1000.,Pktheo[:,-1],label='N = %d'%N)
#             axarr[1].plot(ks/100.,Pktheo[:,-1],label='N = %d'%N)
#         axarr[0].legend(loc='upper left')
#         axarr[0].set_xlim([0,t0/1e6*1.1])
#         axarr[0].set_ylim(ymin=0)
#         axarr[0].set_xlabel('Time / $10^6$')
#         axarr[0].set_ylabel('Channel Number')
#         axarr[0].set_title('$R_\mathrm{ch} = %.0f, Z_\mathrm{ch} = %.0f$'%(Rch,Zch))
#         # axarr[0].tight_layout()
#         # axarr[0].savefig('plots/theomean-%.0f-%.0f.png'%(Rch,Zch),dpi=200)
#         # axarr[0].close()
#         # Plot histograms
#         # axarr[1].legend(loc='upper left')
#         axarr[1].legend()
#         axarr[1].set_ylim(ymin=0)
#         # axarr[1].set_xlim([20,27]) # (30,90)
#         # axarr[1].set_xlim([0,15]) # (90,60)
#         axarr[1].set_xlim([1,8]) # (90,75)
#         # axarr[1].set_xlabel('Thousands of Channels, $k/1000$')
#         axarr[1].set_xlabel('Hundreds of Channels, $k/100$')
#         axarr[1].set_ylabel('Probability')
#         axarr[1].set_title(r'Time $%d \times 10^6$'%int(round(t0/1e6)))
#         # axarr[1].tight_layout()
#         # axarr[1].savefig('plots/theodist-%.1f-%.1f.png'%(Rch,Zch),dpi=100)
#         # axarr[1].close()
#         plt.tight_layout()
#         plt.savefig('plots/theodist-%.0f-%.0f.png'%(Rch,Zch),dpi=200)
#         plt.close()
        


# # Map to spatial distribution by convolutions
# # Regarding zero-padding: http://tinyurl.com/ycvsyv4k
# # Goal: get the distribution of particles over the channel numbers k in [0,kmax] up to time t0
# # rhomacro: the distribution of tmacro for a single pass
# # FPTks[k]: the distribution of the first passage time into channel k
# # CumuPk[k,t]: the probability of having made it through at least k channels by time t
# # Pk[k,t]: the probability of being in channel k at time t
# kmax=1000                         # Number of channels to cross
# ks = np.arange(kmax+1)
# nbins=500                      # Number of bins in the tmacro histogram
# for Rch,dfR_meso in df_meso.groupby('Rchannel'):
#     for Zch,df0_meso in dfR_meso.groupby('Zchannel'):
#         dists = []
#         Ns = np.intersect1d(df_micro.N.unique(),df0_meso.LC.unique())
#         for i,N in enumerate(Ns):
#             casename = '%.0f-%.0f-%d'%(Rch,Zch,N)
#             # Restrict attention to this chain length
#             dfN_micro = df_micro[df_micro.N==N]
#             dfN_meso = df0_meso[df0_meso.LC==N]
#             # Form tmacro sample and histogram it
#             tmacro = (dfN_meso.tmeso.values.reshape(1,-1) +
#                       dfN_micro.t.values.reshape(-1,1)).reshape(-1)
#             # t0 = (kmax+1) * np.max(tmacro) # Must be this big to accomodate zero padding
#             t0 = 100.0 * np.max(tmacro) # Can be smaller, but then must truncate distributions, which can lead to Gibbs crap
#             rhomacro, mybins = np.histogram(tmacro,bins=nbins,range=(0,np.max(tmacro)),normed=True)
#             dt = mybins[1]-mybins[0]
#             rhomacro *= dt      # Make rhomacro a PMF
#             nmax = int(t0/dt)        # Maximum time bin -- truncate beyond this
#             # First passage time distributions for each channel
#             FPTks = np.zeros((kmax+1,int(t0/dt)))
#             tnz = rhomacro.shape[0]      # Number of bins that are non-zero
#             FPTks[0,:tnz] = rhomacro[:nmax]
#             # Compute FPTs one after the other
#             for k in np.arange(kmax):
#                 FPTk = sgnl.fftconvolve(FPTks[k,:tnz],rhomacro)
#                 tnz = FPTk.shape[0]
#                 FPTks[k+1,:tnz] = FPTk[:nmax]
#                 if FPTk.sum()<0.999:
#                     print "Exceeding tolerance at k=%d"%k
#                     break
#             # Compute Pks
#             CumuPks = FPTks.cumsum(axis=1)
#             Pks = -np.gradient(CumuPks,axis=0)
#             # # Plot Pks
#             # for nt in np.arange(nmax)[::50]:
#             #     plt.plot(Pks[:,nt],label='Integral = %.2e'%Pks[:,nt].sum())
#             #     plt.legend()
#             #     plt.xlabel('Channel Number k')
#             #     plt.ylabel('PDF')
#             #     plt.title('Rch = %.0f, Zch = %.0f, N = %d, t = %.1e'%(Rch,Zch,N,nt*dt))
#             #     plt.tight_layout()
#             #     plt.savefig('plots/%s-Pk_%03d.png'%(casename,nt),dpi=100)
#             #     plt.close()
#             # # Plot spacetime distribution!
#             # plt.pcolor(np.arange(kmax+1),
#             #            np.arange(nmax)*dt,
#             #            Pks.T)
#             # plt.xlabel('Channel Number k')
#             # plt.ylabel('Time')
#             # plt.title('Rch = %.0f, Zch = %.0f, N = %d'%(Rch,Zch,N))
#             # plt.tight_layout()
#             # plt.savefig('plots/%s-allPk.png'%casename,dpi=200)
#             # plt.close()
#             # thresh = 1e-5
#             # plt.pcolor(np.arange(kmax+1),
#             #            np.arange(nmax)*dt,
#             #            Pks.T-Pks.max()*thresh, # Remove everything below threshold from log color map -- introduce small error
#             #            norm=colors.LogNorm(vmin=Pks.max()*thresh,vmax=Pks.max()))
#             # plt.xlabel('Channel Number k')
#             # plt.ylabel('Time')
#             # plt.title('Rch = %.0f, Zch = %.0f, N = %d'%(Rch,Zch,N))
#             # plt.tight_layout()
#             # plt.savefig('plots/%s-allPk_log.png'%casename,dpi=200)
#             # plt.close()
#             # sys.exit()
#             # Cumulative plot for each geometry (comment out other plots)
#             # E_ks = (ks[:,np.newaxis]*Pks).sum(axis=0)/Pks.sum(axis=0) # Expected value of k over time
#             # E_ksqrs = (ks[:,np.newaxis]**2*Pks).sum(axis=0)/Pks.sum(axis=0) # Expected value of k^2 over time
#             E_ks = (ks[:,np.newaxis]*Pks).sum(axis=0) # Expected value of k over time
#             E_ksqrs = (ks[:,np.newaxis]**2*Pks).sum(axis=0) # Expected value of k^2 over time
#             Std_ks = (E_ksqrs - E_ks**2)**0.5
#             interval = 2000
#             plt.errorbar(dt*np.arange(nmax)[::interval]/1e6,
#                          E_ks[::interval],
#                          yerr=Std_ks[::interval],
#                          label='N = %d'%N)
#         plt.legend()
#         # plt.xlim([0,2500]) # (30,75)
#         # plt.xlim([0,15000])
#         #plt.ylim([0,kmax*0.75])
#         plt.xlim([0,90])
#         plt.ylim([0,350])
#         plt.xlabel('Time / $10^6$')
#         plt.ylabel('Channel Number')
#         plt.title('$R_\mathrm{ch} = %.0f, Z_\mathrm{ch} = %.0f$'%(Rch,Zch))
#         plt.tight_layout()
#         plt.savefig('plots/mean-%.0f-%.0f.png'%(Rch,Zch),dpi=200)
#         plt.close()
#         # sys.exit()
            

# # Violin plots! (tmacro or Zch/tmacro)
# for Rch,dfR_meso in df_meso.groupby('Rchannel'):
#     for Zch,df0_meso in dfR_meso.groupby('Zchannel'):
#         dists = []
#         Ns = np.intersect1d(df_micro.N.unique(),df0_meso.LC.unique())
#         for i,N in enumerate(Ns):
#             # Restrict attention to this chain length
#             dfN_micro = df_micro[df_micro.N==N]
#             dfN_meso = df_meso[df_meso.LC==N]
#             # Form tmacro sample
#             tmacro = (df0_meso.tmeso.values.reshape(1,-1) +
#                       dfN_micro.t.values.reshape(-1,1)).reshape(-1)
#             # dists.append(np.random.choice(tmacro,1000))
#             dists.append(np.random.choice(Zch/tmacro,1000))
#         plt.violinplot(dataset=dists,positions=Ns,widths=10,points=100,
#                        showmeans=True,showextrema=False,showmedians=False)
#         # plt.yscale('log')
#         plt.xlabel('Chain Length')
#         # plt.ylabel('Macroscopic Time')
#         plt.ylabel('Macroscopic Speed Zch/tmacro')
#         plt.title('Rch = %.1f, Zch = %.1f'%(Rch,Zch))
#         plt.tight_layout()
#         # plt.savefig('plots/violin-tmacro-%.1f-%.1f.png'%(Rch,Zch),dpi=100)
#         plt.savefig('plots/violin-mob-%.1f-%.1f.png'%(Rch,Zch),dpi=100)
#         plt.close()

# # Find tmacro distribution for each (N,Rch,Zch)
# myindex = pd.MultiIndex.from_product([[]]*3,names=['N','Rch','Zch'])
# df_macro = pd.DataFrame(columns=['t_mean','t_std'],index=myindex)
# Ns = np.intersect1d(df_micro.N.unique(),df_meso.LC.unique())
# for N in Ns:
#     # Restrict attention to this chain length
#     dfN_micro = df_micro[df_micro.N==N]
#     dfN_meso = df_meso[df_meso.LC==N]
#     for (Rch,Zch),df0_meso in dfN_meso.groupby(['Rchannel','Zchannel']):
#         # Form tmacro sample
#         tmacro = (df0_meso.tmeso.values.reshape(1,-1) +
#                   dfN_micro.t.values.reshape(-1,1)).reshape(-1)
#         # Plot distributions
#         plt.hist(tmacro,
#                  normed=True,bins=100,histtype='step',label='Macro')
#         plt.hist(df0_meso.tmeso,normed=True,bins=100,histtype='step',label='Meso')
#         plt.hist(dfN_micro.t,normed=True,bins=100,histtype='step',label='Micro')
#         plt.legend()
#         plt.xlabel('Time (Simulation Units)')
#         plt.ylabel('Probability')
#         plt.yscale('log')
#         plt.tight_layout()
#         plt.savefig('plots/tmacro_%d_%.1f_%.1f.png'%(N,Rch,Zch),dpi=200)
#         plt.close()
#         # Track tmacro mean and std dev
#         df_macro.loc[(N,Rch,Zch)] = [tmacro.mean(),tmacro.std()]
# df_macro.reset_index(inplace=True)
# df_macro.to_csv('plots/tmacro.csv')

# # Load the macroscopic data
# df_macro = pd.read_csv("plots/tmacro.csv",index_col=0)
# print("Macroscopic data loaded.")

# # Filter the data
# df_macro['dcap'] = df_macro.N/4.0
# LP = 10.
# LC = df_macro.N
# df_macro['RG'] = ((1./3.)*LP*LC - LP**2 +
#                   (2.*LP**3/LC)*( 1-(LP/LC)*(1-np.exp(-LC/LP))))**0.5
# df_macro = df_macro[df_macro.Rch>df_macro.dcap+df_macro.RG]
# df_macro = df_macro[df_macro.Zch>df_macro.dcap+df_macro.RG]

# # Plot mobilities for each Rch
# for Rch,dfR_macro in df_macro.groupby('Rch'):
#     for Zch,grp in dfR_macro.groupby('Zch'):
#         plt.errorbar(grp.N,Zch/grp.t_mean,
#                      yerr=Zch*grp.t_std/grp.t_mean**2,label=Zch)
#     plt.title('Rch = %.1f'%Rch)
#     plt.legend(title='Zch')
#     plt.ylim(ymin=0)
#     plt.xlabel('N')
#     plt.ylabel('Macroscopic Drift Velocity Zch/tmacro')
#     plt.tight_layout()
#     plt.savefig('plots/mobR_%.1f.png'%Rch,dpi=200)
#     plt.close()

# # Plot mobilities for each Zch
# for Zch,dfZ_macro in df_macro.groupby('Zch'):
#     for Rch,grp in dfZ_macro.groupby('Rch'):
#         plt.errorbar(grp.N,Zch/grp.t_mean,
#                      yerr=Zch*grp.t_std/grp.t_mean**2,label=Rch)
#     plt.title('Zch = %.1f'%Rch)
#     plt.legend(title='Rch')
#     plt.ylim(ymin=0)
#     plt.xlabel('N')
#     plt.ylabel('Macroscopic Drift Velocity Zch/tmacro')
#     plt.tight_layout()
#     plt.savefig('plots/mobZ_%.1f.png'%Zch,dpi=200)
#     plt.close()

# # Plot std dev for each Rch
# for Rch,dfR_macro in df_macro.groupby('Rch'):
#     for Zch,grp in dfR_macro.groupby('Zch'):
#         plt.plot(grp.N,grp.t_std,label=Zch)
#     plt.title('Rch = %.1f'%Rch)
#     plt.legend(title='Zch')
#     plt.ylim(ymin=0)
#     plt.xlabel('N')
#     plt.ylabel('Std Dev of Macroscopic Time')
#     plt.tight_layout()
#     plt.savefig('plots/stdmacro_%.1f.png'%Rch,dpi=200)
#     plt.close()
