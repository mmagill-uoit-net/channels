import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.colors as colors
import sys
import glob
import scipy.stats as sps

mpl.rcParams['font.size']=18
mpl.rcParams['figure.figsize']=(4,5)
mpl.rcParams['lines.linewidth']=3


datadir = "data/"
# LCs = [10,20,50,75,100,150,200,300]
# Rchannels = [30,45,60,75,90,150,300]
# Zchannels = [30,45,60,75,90,150,300]
# Rchannels = [50,100,500]
# Zchannels = [500,1000]

Npart=10240
rseed=1
OutFreq=int(1e5)




LCs = [10]
Rchannels = [30]
Zchannels = [30]
### 2017/04/20: Heatmaps worthy of a poster
for Rchannel in Rchannels:
    for Zchannel in Zchannels:
        for LC in LCs:
            foutname = (datadir+"/heatmap_%d_%.1f_%.1f_%d_%d_%d.npy"%
                        (LC,Rchannel,Zchannel,Npart,rseed,OutFreq))
            print foutname
            data = np.load(foutname)
            ncols = np.unique(data[:,0]).shape[0]
            R = data[:,0].reshape(-1,ncols)
            Z = data[:,1].reshape(-1,ncols)
            H = data[:,2].reshape(-1,ncols)
            AR = float(Zchannel)/float(Rchannel)
            w_in = 4
            # fig, ax = plt.subplots(figsize=(w_in,w_in*AR))
            # mappable = ax.pcolor(R,Z,H/(R+R[0,1]),norm=colors.LogNorm(vmin=1, vmax=H.max()))
            # # plt.colorbar(mappable,ax=ax)
            # # ax.set_xlabel("R")
            # # ax.set_ylabel("Z")
            # ax.get_xaxis().set_visible(False)
            # ax.get_yaxis().set_visible(False)
            # ax.set_aspect('equal')
            # plt.subplots_adjust(top=1.0,bottom=0.0,left=0.0,right=1.0)
            # plt.savefig("plots/heatmap_log_renorm_%.1f_%.1f_%d_%d_%d_%d.eps"%
            #             (Rchannel,Zchannel,LC,Npart,rseed,OutFreq),
            #             dpi=300)
            # plt.close()
            fig, axarr = plt.subplots(1,2,figsize=(2*w_in,w_in*AR))
            ax = axarr[0]
            mappable = ax.pcolor(R,Z,H/(R+R[0,1]),rasterized=True)
            ax.contour(R,Z,
                       (Z-0.5*Zchannel)**2+R**2-(LC/4.0)**2,
                       [0],colors='w')
            # plt.colorbar(mappable,ax=ax)
            # ax.set_xlabel("R")
            # ax.set_ylabel("Z")
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            ax.set_aspect('equal')
            ax.text(25,10,'(a)',color='w')
            # plt.arrow(20,-10,0,5,color='w',head_width=1)
            # plt.arrow(20,-10,5,0,color='w',head_width=1)
            # plt.text(20+5,-10+1,'r',color='w')
            # plt.text(20+1,-10+5,'z',color='w')
            # plt.subplots_adjust(top=1.0,bottom=0.0,left=0.0,right=1.0)
            # plt.savefig("plots/heatmap_renorm_%.1f_%.1f_%d_%d_%d_%d.png"%
            #             (Rchannel,Zchannel,LC,Npart,rseed,OutFreq),
            #             dpi=300)
            # plt.savefig("plots/heatmap_renorm_%.1f_%.1f_%d_%d_%d_%d.tiff"%
            #             (Rchannel,Zchannel,LC,Npart,rseed,OutFreq),
            #             dpi=300)
            # plt.close()



LCs = [50]
Rchannels = [30]
Zchannels = [30]
ax = axarr[1]
### 2017/04/20: Heatmaps worthy of a poster
for Rchannel in Rchannels:
    for Zchannel in Zchannels:
        for LC in LCs:
            try:
                foutname = (datadir+"/heatmap_%d_%.1f_%.1f_%d_%d_%d.npy"%
                            (LC,Rchannel,Zchannel,Npart,rseed,OutFreq))
                print foutname
                data = np.load(foutname)
                ncols = np.unique(data[:,0]).shape[0]
                R = data[:,0].reshape(-1,ncols)
                Z = data[:,1].reshape(-1,ncols)
                H = data[:,2].reshape(-1,ncols)
                AR = float(Zchannel)/float(Rchannel)
                w_in = 4
                # fig, ax = plt.subplots(figsize=(w_in,w_in*AR))
                # mappable = ax.pcolor(R,Z,H/(R+R[0,1]),norm=colors.LogNorm(vmin=1, vmax=H.max()))
                # # plt.colorbar(mappable,ax=ax)
                # # ax.set_xlabel("R")
                # # ax.set_ylabel("Z")
                # ax.get_xaxis().set_visible(False)
                # ax.get_yaxis().set_visible(False)
                # ax.set_aspect('equal')
                # plt.subplots_adjust(top=1.0,bottom=0.0,left=0.0,right=1.0)
                # plt.savefig("plots/heatmap_log_renorm_%.1f_%.1f_%d_%d_%d_%d.eps"%
                #             (Rchannel,Zchannel,LC,Npart,rseed,OutFreq),
                #             dpi=300)
                # plt.close()
                mappable = ax.pcolor(R,Z,H/(R+R[0,1]),rasterized=True)
                ax.contour(R,Z,
                           (Z-0.5*Zchannel)**2+R**2-(LC/4.0)**2,
                            [0],colors='w')
                # plt.colorbar(mappable,ax=ax)
                # ax.set_xlabel("R")
                # ax.set_ylabel("Z")
                ax.get_xaxis().set_visible(False)
                ax.get_yaxis().set_visible(False)
                ax.set_aspect('equal')
                plt.text(25,10,'(b)',color='w')
                plt.arrow(20,-10,0,5,color='w',head_width=1)
                plt.arrow(20,-10,5,0,color='w',head_width=1)
                plt.text(20+5,-10+1,'r',color='w')
                plt.text(20+1,-10+5,'z',color='w')
                plt.subplots_adjust(top=1.0,bottom=0.0,left=0.0,right=1.0,wspace=0,hspace=0)
                plt.savefig("plots/heatmap_renorm_%.1f_%.1f_%d_%d_%d_%d.eps"%
                            (Rchannel,Zchannel,LC,Npart,rseed,OutFreq),
                            dpi=300)
                # plt.savefig("plots/heatmap_renorm_%.1f_%.1f_%d_%d_%d_%d.png"%
                #             (Rchannel,Zchannel,LC,Npart,rseed,OutFreq),
                #             dpi=300)
                # plt.savefig("plots/heatmap_renorm_%.1f_%.1f_%d_%d_%d_%d.tiff"%
                #             (Rchannel,Zchannel,LC,Npart,rseed,OutFreq),
                #             dpi=300)
                plt.close()
            except:
                pass    
