import pandas as pd
import glob

pd.concat([pd.read_csv(file,index_col=0) for file in glob.glob("data/times*.csv")]).to_csv("gathered-channels-meso.csv")
