from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import pandas as pd
import scipy as sp
import scipy.interpolate
import scipy.stats
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import Wedge
import matplotlib.colors as colors
import time
import sys
from numba import cuda
import math

maxuint64=np.iinfo(np.uint64).max

## CLI ##
LC       = float(sys.argv[1])
Rchannel = float(sys.argv[2])
Zchannel = float(sys.argv[3])

## Notes ##
# Coordinates are centered at the middle of the channels
print("Notes\n---")
print("---")

ttotal = time.time()
######### Initialization #########
# Seed random number generator
rseed=1
np.random.seed(rseed)

# Numerical Parameters
Npart = 10240
print("Running with %d particles."%Npart)
dt = np.float32(0.5)
#timestep_cutoff = int(1e5)

# Flags
PlotFlag=0
GridFlag=0
QAFlag=1
OutFlag=1
OutFreq=int(1e5)

# Geometry
Reff = Rchannel
Zeff = Zchannel/2
Rpore = 0.8    # Radius of pore
Zpore = 1.0/2. # Half length of pore
Rmax = Rchannel
Zmax = Zchannel+2*Zpore
Zcross = np.float32(Zeff*0.75) # Position of the crossing line
posx_pore = np.float32(0.)
posy_pore = np.float32(0.)
posz_pore = np.float32(Zeff)
posx_cap  = np.float32(0.)
posy_cap  = np.float32(0.)
posz_cap  = np.float32(Zmax/2.)
posx_reset = np.float32(0.)
posy_reset = np.float32(0.)
posz_reset = np.float32(-Zeff)

# Field
res_mesh = 200 # Solution mesh resolution
FineZone = 3   # Size of refined mesh region
Nr = 200       # Interpolated mesh res in r
Nz = 400       # Interpolated mesh res in r
kTsim = 1.
muexp = 3.14e-8
dVexp = 0.200
LCtune = 100
sigmaexp = 5e-9
Dexp = 2.38e-12/(LCtune*sigmaexp*1e6)**0.608
gain = kTsim*muexp*dVexp/(LCtune*Dexp)

# Particle properties
D    = 1.0/LC # Improve this
d_cap = 0.25*LC # Match to channels-micro

# Initialize arrays
posx  = np.zeros((Npart,)) + posx_reset
posy  = np.zeros((Npart,)) + posy_reset
posz  = np.zeros((Npart,)) + posz_reset
dposx = 0.*posx
dposy = 0.*posy
dposz = 0.*posz
capflag = np.zeros(posx.shape[0],dtype=np.int32)
crossflag = np.zeros(posx.shape[0],dtype=np.int32)
particle_timestep = np.zeros(posx.shape[0],dtype=np.int32) # Each particle has its own clock
tcross = np.zeros(posx.shape[0],dtype=np.float32) # FPT to crossing line
tmeso = np.zeros(posx.shape[0],dtype=np.float32)  # Total trajectory time

######### End Initialization #########


########## Load Force Field ##########
fielddir = "field/data/"
QAdir = "field/QA/"
QAdpi = 400
try:
    U_load = np.loadtxt(fielddir+"/U_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.npy"%
                        (Rpore,Zpore,Rchannel,Zchannel,res_mesh,Nr,Nz))
    print("Field found and loaded.")
except:
    print("Field not found. Generating it now.")
    sys.path.insert(0, 'field/')
    import MM_FEniCS_ChannelsMeso
    MM_FEniCS_ChannelsMeso.SolvePotential(Rpore,Zpore,
                                           Rchannel,Zchannel,
                                           res_mesh,FineZone,
                                           Nr,Nz,
                                           fielddir,QAdir,QAdpi)
    U_load = np.loadtxt(fielddir+"/U_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.npy"%
                        (Rpore,Zpore,Rchannel,Zchannel,res_mesh,Nr,Nz))
r = U_load[:,0]
z = U_load[:,1]
u = U_load[:,2] * gain   # Apply gain here
print("Applied a gain of %f"%gain)
ncols = np.unique(r).shape[0]
R = r.reshape(-1,ncols)
Z = z.reshape(-1,ncols)
U = u.reshape(-1,ncols)
dr = R[0,1]-R[0,0]
dz = Z[1,0]-Z[0,0]
Nr = R.shape[1] # This is 1 higher than input bc of fnx.RectangleMesh
Nz = R.shape[0]
print("Field resolution is (dr,dz)=(%.2f,%.2f)"%(dr,dz))
F = np.gradient(U,dz,dr) # each row of U is at constant z
F = [F[1],F[0]]          # So that Fr is first
Fr = np.ascontiguousarray(F[0].ravel().astype(np.float32))
Fz = np.ascontiguousarray(F[1].ravel().astype(np.float32)) 
Zmin = Z.min()           # For NN field application

print("Field loaded.")
########## End Load Force Field ##########


########## Initialize Output ##########
suffix = "%d_%.1f_%.1f_%d_%d_%d"%(LC,Rchannel,Zchannel,Npart,rseed,OutFreq)

# Integrated heat map of all particles positions
heatmap = np.zeros((Nz,Nr),dtype=np.float32)
heatmap = heatmap.ravel()
fheatmapname = ("data/heatmap_%s.npy"%suffix)

# Time statistics
df_times = pd.DataFrame()
ftimesname = ("data/times_%s.csv"%suffix)

########## End Initialize Output ##########


########## Functions ##########

## http://tinyurl.com/kotjjo8
@cuda.jit(device=True)
def cuda_xorshiftstar(states, id):
    x = states[id]
    x ^= x >> 12
    x ^= x << 25
    x ^= x >> 27
    states[id] = x
    return np.uint64(x) * np.uint64(2685821657736338717)


@cuda.jit()
def CUDA_ApplyField(dposx,dposy,dposz,posx,posy,posz,capflag,Fr,Fz,states,randnums,heatmap,crossflag,particle_timestep,tcross,tmeso):
    # i is particle index, which I think corresponds to a given thread
    i = cuda.grid(1)
    # If ever indexing particles that don't exist, ignore them
    if i >= dposx.shape[0]:
        return

    # Main loop
    for timestep in range(OutFreq):

        # Ignore captured particles
        if capflag[i]>0:
            pass

        # Free particles
        if capflag[i]==0:

            # Increment time
            particle_timestep[i] += 1

            # Diffusion
            randnums[i] = cuda_xorshiftstar(states,i) # Update random number list
            dposx[i] = ( math.sqrt(12.)*( (float(randnums[i])/float(maxuint64))-0.5 ) *
                         math.sqrt(2.0*D*dt) )
            randnums[i] = cuda_xorshiftstar(states,i)
            dposy[i] = ( math.sqrt(12.)*( (float(randnums[i])/float(maxuint64))-0.5 ) *
                         math.sqrt(2.0*D*dt) )
            randnums[i] = cuda_xorshiftstar(states,i)
            dposz[i] = ( math.sqrt(12.)*( (float(randnums[i])/float(maxuint64))-0.5 ) *
                         math.sqrt(2.0*D*dt) )

            # Electric field
            posr = (posx[i]**2.+posy[i]**2.)**0.5
            posi = int(((posz[i]-Zmin)/dz)+0.5)
            posj = int((posr/dr)+0.5)
            ind = posi*Nr + posj
            curFr = Fr[ind]
            curFz = Fz[ind]
            curFx = curFr * posx[i] / (posr+1e-6)
            curFy = curFr * posy[i] / (posr+1e-6)
            dposx[i] += dt*LC*curFx*D
            dposy[i] += dt*LC*curFy*D
            dposz[i] += dt*LC*curFz*D

            # Heat Map
            heatmap[ind] += 1

            # Z Walls
            posz_fake = posz[i]+dposz[i]
            if ( posz_fake > +Zeff ):
                dposz[i] *= -1.
                dposz[i] += 2.*(+Zeff-posz[i])
            if ( posz_fake < -Zeff ):
                dposz[i] *= -1.
                dposz[i] += 2.*(-Zeff-posz[i])

            # R Walls
            # Approximate points crossing Reff as being on Reff
            posx_fake = posx[i]+dposx[i]
            posy_fake = posy[i]+dposy[i]
            posr_fake = math.sqrt(posx_fake**2.+posy_fake**2.)
            if ( posr_fake > Reff ):
                rfac1 = (posy[i]**2.-posx[i]**2.)/Reff**2.
                rfac2 = 2.*posx[i]*posy[i]/Reff**2.
                dposx_old = dposx[i]
                dposy_old = dposy[i]
                dposx[i] = +rfac1*dposx_old - rfac2*dposy_old
                dposy[i] = -rfac2*dposx_old - rfac1*dposy_old

            # Update positions
            posx[i] += dposx[i]
            posy[i] += dposy[i]
            posz[i] += dposz[i]

            # Capture
            d_to_pore = math.sqrt( (posx[i]-posx_pore)**2 +
                                   (posy[i]-posy_pore)**2 +
                                   (posz[i]-posz_pore)**2 )
            if ( d_to_pore < d_cap ):
                # Place particle in the pore
                posx[i] = posx_cap
                posy[i] = posy_cap
                posz[i] = posz_cap

                # Record tmeso
                tmeso[i] = particle_timestep[i] * dt

                # Particle is now captured
                capflag[i] = 1
                
            # Cross (check after capture, as capture is treated as crossing too)
            if ( (crossflag[i] == 0) & ( posz[i] > Zcross ) ):
                tcross[i] = particle_timestep[i] * dt
                crossflag[i] = 1



########## End Functions ##########


########## Main Loop ##########

# Initialize plotting
if PlotFlag==2:
    fig, axarr = plt.subplots(1,2,figsize=(15,7))
if PlotFlag==3:
    fig3d = plt.figure(); ax3d = fig3d.add_subplot(111, projection='3d')


# TPB of 32 seemed to be best for perfectly parallel application
tpb = 32
bpg = int(np.ceil(Npart/tpb))

# Initialize RNG
states = np.arange(tpb*bpg, dtype=np.uint64)+1 # 1 state per thread
randnums = 0*states

# Send to CUDA in groups of OutFreq timesteps
#for timestep_group in range(timestep_cutoff/OutFreq):
loop_number = 0
while ( capflag.sum() < Npart ):

    # Main calculation in CUDA
    loop_number += 1
    tloop = time.time()
    CUDA_ApplyField[bpg,tpb](dposx,dposy,dposz,
                             posx,posy,posz,
                             capflag,Fr,Fz,
                             states,randnums,
                             heatmap,
                             crossflag,particle_timestep,
                             tcross,tmeso)
    #timestep = (timestep_group+1)*OutFreq
    dtloop = time.time()-tloop
    if QAFlag==1:
        print("%d timesteps completed:\n"%(loop_number*OutFreq) +
              "\tLast %d timesteps took %.2fs\n"%(OutFreq,dtloop) +
              "\t%d%% of the particles have crossed\n"%(float(100.*crossflag.sum())/float(Npart)) +
              "\t%d%% of the particles are captured\n"%(float(100.*capflag.sum())/float(Npart)) +
              "\tRemaining free particles: %d."%(Npart-capflag.sum()) )

    # Output
    if OutFlag==1:
        # Heatmap
        np.save(fheatmapname,np.vstack([r,z,heatmap]).T)

        #Times
        df_times = pd.DataFrame(data=np.vstack([tmeso,tcross,tmeso-tcross]).T,columns=['tmeso','tcross','tcap'])
        df_times['LC'] = LC
        df_times['Rchannel'] = Rchannel
        df_times['Zchannel'] = Zchannel
        df_times = df_times[['LC','Rchannel','Zchannel','tmeso','tcross','tcap']]
        df_times.to_csv(ftimesname)
        
        


    # Plotting
    if PlotFlag==2:
        axarr[0].quiver(posx-dposx,posz-dposz,dposx,dposz,
                        angles='xy',scale_units='xy',scale=1)
        axarr[1].quiver(posy-dposy,posz-dposz,dposy,dposz,
                        angles='xy',scale_units='xy',scale=1)
    if PlotFlag==3:
        ax3d.quiver(posx,posy,posz,
                    dposx,dposy,dposz)


print("Main loop finished.")
########## End Main Loop ##########



######### Plot Formatting #########
if PlotFlag==1:
    Heatmap = heatmap.reshape(Nz,Nr)
    fig, axarr = plt.subplots(1,2,figsize=(11,7))
    mappable = axarr[0].pcolor(R,Z,Heatmap/(R+R[0,1]))
    plt.colorbar(mappable,ax=axarr[0])
    mappable = axarr[1].pcolor(R,Z,Heatmap/(R+R[0,1]),norm=colors.LogNorm(vmin=1, vmax=Heatmap.max()))
    plt.colorbar(mappable,ax=axarr[1])
    for ax in axarr:
        ax.set_xlabel("R")
        ax.set_ylabel("Z")
        ax.set_aspect('equal')
        ax.autoscale(tight=True)
    
if PlotFlag==2:
    axarr[0].set_xlabel('x')
    axarr[1].set_xlabel('y')
    Rplot = Rmax*1.1
    Zplot = (Zmax/2+Zpore)*1.1
    for ax in axarr:
        ax.set_ylabel('z')
        ax.axis('equal')
        # Draw channel
        ax.set_xlim([-Rplot,Rplot])
        ax.axvline(-Rchannel)
        ax.axvline(+Rchannel)
        ax.set_ylim([-Zplot,Zplot])
        ax.axhline(-Zchannel/2)
        ax.axhline(+Zchannel/2)
        ax.axhline(-Zmax/2)
        ax.axhline(+Zmax/2)
        ax.axhline(-(Zmax/2+Zpore))
        ax.axhline(+(Zmax/2+Zpore))
        # Draw pores
        ax.plot([+Rpore,+Rpore],[+(Zmax/2-Zpore),+(Zmax/2+Zpore)],'C0')
        ax.plot([-Rpore,-Rpore],[+(Zmax/2-Zpore),+(Zmax/2+Zpore)],'C0')
        ax.plot([+Rpore,+Rpore],[-(Zmax/2-Zpore),-(Zmax/2+Zpore)],'C0')
        ax.plot([-Rpore,-Rpore],[-(Zmax/2-Zpore),-(Zmax/2+Zpore)],'C0')
        # Draw capture radius
        wedge = Wedge((0.,Zchannel/2.), d_cap, 180, 360, fc='none', ec='k')
        ax.add_artist(wedge)
        # Draw field mesh
        if GridFlag==1:
            ax.grid(True, which='minor', axis='both', linestyle='-', color='k')
            xy = np.hstack([-r,r[1:]])
            ax.set_xticks(xy, minor=True)
            ax.set_yticks(z, minor=True)

# Show
plt.show()

######### End Plot Formatting #########
dttotal = time.time() - ttotal
print("\nTotal Runtime: %.2e s"%dttotal)



