import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import sys
import glob
import scipy.stats as sps
import scipy.signal as sgnl
import scipy.special as spec

mpl.rcParams['font.size']=18
mpl.rcParams['figure.figsize']=(8,6)
mpl.rcParams['lines.linewidth']=3



# Load the microscopic data
df_micro = pd.read_csv("../micro/gathered-channels-micro.csv",index_col=0)
print("Microscopic data loaded.")

# Load the mesoscopic data
df_meso = pd.read_csv("gathered-channels-meso.csv",index_col=0)
df_meso['dcap'] = df_meso.LC/4.0
LP = 10.
LC = df_meso.LC
df_meso['RG'] = ((1./3.)*LP*LC - LP**2 +
                 (2.*LP**3/LC)*( 1-(LP/LC)*(1-np.exp(-LC/LP))))**0.5
df_meso = df_meso[df_meso.Rchannel>df_meso.dcap+df_meso.RG]
df_meso = df_meso[df_meso.Zchannel>df_meso.dcap+df_meso.RG]

df_meso = df_meso[df_meso.Rchannel<1000]
df_meso = df_meso[df_meso.Zchannel<1000]

# Plots for paper
df_meso = df_meso[((df_meso.Rchannel==90)&(df_meso.Zchannel==75))]
print("Mesoscopic data loaded.")



# CLT approximation for spatial distribution
for Rch,dfR_meso in df_meso.groupby('Rchannel'):

    for Zch,df0_meso in dfR_meso.groupby('Zchannel'):

        Ns = np.intersect1d(df_micro.N.unique(),df0_meso.LC.unique())
        mobs = []
        
        fig, axarr = plt.subplots(2,1,figsize=(8,12))

        t0 = 3e7                # (90,60)
        kmax = 5e3
        ks = np.arange(1,kmax)
        ts = np.linspace(0,t0,50)
        TT, KK = np.meshgrid(ts,ks)

        for N in Ns:

            # Restrict attention to this chain length
            dfN_micro = df_micro[df_micro.N==N]
            dfN_meso = df0_meso[df0_meso.LC==N]

            # Statistics
            mu_micro = dfN_micro.t.mean()
            var_micro = dfN_micro.t.var()
            mu_meso = dfN_meso.tmeso.mean()
            var_meso = dfN_meso.tmeso.var()

            mobs.append(1./(mu_micro+mu_meso))
            
            Pktheo = 0.5*((TT+KK*(mu_micro+mu_meso))*
                          np.exp(-(TT-KK*(mu_micro+mu_meso))**2/
                                 (2*KK*(var_micro+var_meso)))/
                          (2.0*np.pi*KK**3*(var_micro+var_meso))**0.5)

            E_ks = (KK*Pktheo).sum(axis=0) # Expected value of k over time
            E_ksqrs = (KK**2*Pktheo).sum(axis=0) # Expected value of k^2 over time
            Std_ks = (E_ksqrs - E_ks**2)**0.5

            line, _, _ = axarr[0].errorbar(ts/1e6,
                                     E_ks,
                                     yerr=Std_ks,
                                     label='N = %d'%N)
            if N==10:
                axarr[0].text(t0/1e6*1.02,E_ks[-1]-30,'N = %d'%N,color=line.get_color())
            elif N==50:
                axarr[0].text(t0/1e6*1.02,E_ks[-1]-15,'N = %d'%N,color=line.get_color())
            elif N==200:
                axarr[0].text(t0/1e6*1.02,E_ks[-1]-15,'N = %d'%N,color=line.get_color())
            else:
                axarr[0].text(t0/1e6*1.02,E_ks[-1],'N = %d'%N,color=line.get_color())
                
            axarr[1].plot(ks/100.,Pktheo[:,-1],label='N = %d'%N)
            argkpeak = Pktheo[:,-1].argmax()
            kpeak = ks[argkpeak]
            Pkpeak = Pktheo[argkpeak,-1]
            if N==50:
                axarr[1].text(kpeak/100-0.2,Pkpeak+0.0005,'N = %d'%N,color=line.get_color())
            elif N==10:
                axarr[1].text(kpeak/100+0.15,Pkpeak-0.0005,'N = %d'%N,color=line.get_color())
            elif N==20:
                axarr[1].text(kpeak/100+0.15,Pkpeak-0.0005,'N = %d'%N,color=line.get_color())
            elif N==75:
                axarr[1].text(kpeak/100-0.475,Pkpeak+0.0005,'N = %d'%N,color=line.get_color())
            elif N==100:
                axarr[1].text(kpeak/100+0.45,Pkpeak-0.005,'N = %d'%N,color=line.get_color())
            else:
                axarr[1].text(kpeak/100+0.1,Pkpeak,'N = %d'%N,color=line.get_color())



        subax = plt.axes([.235, .775, .235, .16])
        mobs = np.array(mobs)
        plt.plot(Ns,mobs*1e6,'k*-')
        plt.text(15,15,r'$\frac{10^6}{\mu_\mathrm{macro}}$',fontsize=18)
        plt.xlim([0,210])
        plt.ylim([5,20])
        plt.xlabel('N',fontsize=16,labelpad=-10)
        plt.setp(subax.get_xticklabels(), fontsize=14)
        plt.setp(subax.get_yticklabels(), fontsize=14)
        
                
        axarr[0].set_xlim([0,t0/1e6*1.25])
        axarr[0].set_ylim(ymin=0)
        axarr[0].set_xlabel('Time / $10^6$')
        axarr[0].set_ylabel('Channel Number')
        axarr[0].set_title('$R_\mathrm{ch} = %.0f, Z_\mathrm{ch} = %.0f$'%(Rch,Zch))

        axarr[1].set_ylim(ymin=0)
        axarr[1].set_xlim([1,7]) # (90,75)
        axarr[1].set_xlabel('Hundreds of Channels, $k/100$')
        axarr[1].set_ylabel('Probability')
        axarr[1].set_title(r'Time $%d \times 10^6$'%int(round(t0/1e6)))
        plt.tight_layout()
        plt.savefig('plots/theodist-%.0f-%.0f.eps'%(Rch,Zch),dpi=300)
        plt.close()
        
