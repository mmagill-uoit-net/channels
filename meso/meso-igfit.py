import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.colors as colors
import sys
import glob
import scipy.stats as sps

mpl.rcParams['font.size']=18
mpl.rcParams['figure.figsize']=(6,5)
mpl.rcParams['lines.linewidth']=3


datadir = "data/"
LCs = [10,20,50,75,100,150,200,300]
# Rchannels = [30,45,60,75,90,150,300]
# Zchannels = [30,45,60,75,90,150,300]
Rchannels = [50,100,500]
Zchannels = [500,1000]

Npart=10240
rseed=1
OutFreq=int(1e5)





### 2017/05/02: Unified IG fitter for all three times
df = pd.concat([pd.read_csv(file,index_col=0) for file in glob.glob(datadir+"/times*.csv")])
params_all = pd.DataFrame(columns=['LC','Rchannel','Zchannel',
                                   'mymu','mylambda', # Manual guess for IG params
                                   'pymu','pylambda', # Manual guess for python values
                                   'pymu_fit','pylambda_fit', # Fitted values
                                   'realmean','realstd']) # Actual data's mean and std
for tname in ['tcross','tcap','tmeso']:
    i=0
    for Rchannel in Rchannels:
        for Zchannel in Zchannels:
            for LC in LCs:
                # Select data of interest
                dfR = df[(df['Rchannel']==Rchannel)&(df['Zchannel']==Zchannel)]
                dfRLC = dfR[dfR['LC']==LC]
                
                if dfRLC.empty == False:
                    # Histogram of data
                    mymin = dfRLC[tname].min()
                    mymax = dfRLC[tname].max()
                    myptp = mymax-mymin
                    nbins = 30 # Max nbins (approx; excludes buffer bins)
                    bs = np.ceil(float(myptp+1)/float(nbins))
                    dfRLC.hist(tname,bins=np.arange(mymin-bs,(mymax+bs)+bs+1,bs)-0.5,normed=True)
                    
                    # Fitting to IG distribution
                    mymu = dfRLC[tname].mean() # mu of real IG satisfies this
                    try:
                        mylambda = mymu**3. / dfRLC[tname].var() # lambda of real IG satisfies this -- THIS WORKS BETTER THAN MLE
                        # mylambda = 1. / (np.mean(dfRLC[tname]**(-1) - mymu**-1)) # MLE
                    except:
                        # Sometimes var will be zero
                        mylambda = 1
                    pymu = mymu/mylambda # Python mu satisfies this
                    pylambda = mylambda # Python lambda is the same
                    params = sps.invgauss.fit(dfRLC[tname],pymu,floc=0,scale=mylambda) # Primed
                    params_all.loc[i] = np.array([LC,Rchannel,Zchannel,
                                                  mymu,mylambda,
                                                  pymu,pylambda,
                                                  params[0],params[2],
                                                  dfRLC[tname].mean(),dfRLC[tname].std()])
        
                    # Plot fit on top of histogram and save
                    rv = sps.invgauss(mu=params[0],loc=params[1],scale=params[2])
                    rv_naive = sps.invgauss(mu=pymu,loc=0,scale=pylambda)
                    x = np.linspace(dfRLC[tname].min(),dfRLC[tname].max(),500)
                    plt.plot(x,rv.pdf(x))
                    plt.plot(x,rv_naive.pdf(x),'k--')
                    plt.yscale('log', nonposy='clip')
                    plt.ylim(bottom=0.5*1./(bs*Npart))
                    plt.title("N = %d"%LC)
                    plt.ylabel("Probability")
                    plt.xlabel("Time, Simulation Units")
                    plt.tight_layout()
                    plt.savefig("plots/%s_%d_%.1f_%.1f_%d_%d_%d.png"%
                               (tname,LC,Rchannel,Zchannel,Npart,rseed,OutFreq))
                    plt.close()
        
                    # Increment output row
                    i += 1
                    print i
    params_all.to_csv("plots/%s_IGfits.csv"%tname)
    

