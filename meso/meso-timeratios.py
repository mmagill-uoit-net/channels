import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.colors as colors
import sys
import glob
import scipy.stats as sps

mpl.rcParams['font.size']=18
mpl.rcParams['figure.figsize']=(8,6)
mpl.rcParams['lines.linewidth']=3


datadir = "data/"
LCs = [10,20,50,75,100,150,200,300]
# Rchannels = [30,45,60,75,90,150,300]
# Zchannels = [30,45,60,75,90,150,300]
Rchannels = [50,100,500]
Zchannels = [500,1000]

Npart=10240
rseed=1
OutFreq=int(1e5)





### 2017/07/19: Fraction of tmeso that is tcross
df = pd.concat([pd.read_csv(file,index_col=0) for file in glob.glob(datadir+"/times*.csv")])
for key_LC,grp_LC in df.groupby('LC'):
    fig = plt.figure()
    for key,grp in grp_LC.groupby(['Rchannel','Zchannel']).apply(
            lambda x: x.tcross/x.tmeso).mean(axis=1).reset_index().groupby('Rchannel'):
        plt.plot(grp.Zchannel,grp[0],'-*',label=key)
    # Line where dcap=Zchannel-Zcross=Zchannel/8
    d_cap = key_LC/4.0
    Zch_thresh = d_cap*8.0
    plt.axvline(Zch_thresh,color='k',linestyle='--')
    plt.text(100,0.05,'Line: d_cap=Zch-Zcross')
    # Formatting
    plt.title('N = %d'%key_LC)
    plt.xlabel('Zchannel')
    plt.ylabel('tcross/tmeso')
    plt.ylim([0,1])
    # plt.tight_layout()
    # Legend outside the axes
    box = plt.gca().get_position()
    plt.gca().set_position([box.x0, box.y0, box.width * 0.8, box.height])
    plt.legend(title='Rch', loc='upper right',bbox_to_anchor=(1.4, 0.85))
    # Save
    plt.savefig('plots/timeratio-%d.png'%key_LC)
    plt.close()
