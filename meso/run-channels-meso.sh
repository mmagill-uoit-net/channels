#!/bin/bash



source activate fenics2mshrnumba

export CUDA_VISIBLE_DEVICES=0

for Rchannel in 50 100 500 1000
do
    for Zchannel in 500 1000
    do
	for LC in 10 20 50 75 100 150 200 300
	do
	    python channels-meso.py $LC $Rchannel $Zchannel
	done
    done
done


# for Rchannel in 30 45 60 75 90 150 300
# do
#     for Zchannel in 30 45 60 75 90 90 150 300
#     do
# 	for LC in 10 20 50 75 100 150 200 300
# 	do
# 	    python channels-meso.py $LC $Rchannel $Zchannel
# 	done
#     done
# done

