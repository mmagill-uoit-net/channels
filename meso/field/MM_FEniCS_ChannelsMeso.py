from __future__ import print_function
import fenics as fnx
import mshr
import numpy as np
import matplotlib.pyplot as plt


# Parameters
# Rpore     # Pore radius
# Zpore     # Half length
# Rchannel  # Channel radius
# Zchannel  # Channel full length
# res_mesh  # Resolution of full mesh
# Nr        # Resolution of rect mesh in r
# Nz        # Resolution of rect mesh in z
# fielddir  # Where to save output

print("The conda setup required to use fenics seems to be responsible for the annoying Glib-GIO message.")


def SolvePotential(Rpore=0.8,    Zpore=0.5,
                   Rchannel=30., Zchannel=45.,
                   res_mesh=200,FineZone=3,
                   Nr=200,Nz=200,
                   fielddir="field/data/",
                   QAdir="field/QA/",QAdpi=300):
    # Rpore=0.8
    # Zpore=0.5
    # Rchannel=50.
    # Zchannel=120.
    # FineZone=3
    # res_mesh=400
    # Nr=200
    # Nz=200
    # fielddir="data/"
    # QAdir="QA/"
    # QAdpi=400
    
    # Create mesh
    pos_por1BL = fnx.Point(0,0)
    pos_por1UR = fnx.Point(Rpore,Zpore)
    pos_chnlBL = fnx.Point(0,Zpore)
    pos_chnlUR = fnx.Point(Rchannel,Zpore+Zchannel)
    pos_por2BL = fnx.Point(0,Zpore+Zchannel)
    pos_por2UR = fnx.Point(Rpore,Zpore+Zchannel+Zpore)
    rect1 = mshr.Rectangle(pos_por1BL,pos_por1UR)
    rect2 = mshr.Rectangle(pos_chnlBL,pos_chnlUR)
    rect3 = mshr.Rectangle(pos_por2BL,pos_por2UR)
    geo = rect1 + rect2 + rect3
    mesh = mshr.generate_mesh(geo,res_mesh)
    
    # Refine mesh near pores
    Zmax=Zchannel+2*Zpore
    cell_markers = fnx.CellFunction("bool", mesh)
    cell_markers.set_all(False)
    for cell in fnx.cells(mesh):
        vertices = cell.get_vertex_coordinates().reshape(3,2)
        if ( (vertices[:,1].max()<FineZone*Zpore) and (vertices[:,0].max()<FineZone*Rpore) ):
            cell_markers[cell] = True
        if ( (vertices[:,1].min()>(Zmax-FineZone*Zpore)) and (vertices[:,0].max()<FineZone*Rpore) ):
            cell_markers[cell] = True
    mesh = fnx.refine(mesh, cell_markers)
    
    # Define function space
    V = fnx.FunctionSpace(mesh, 'P', 2)
    
    # Define boundary conditions (Homogeneous Newmann BCs by default)
    Zmax = Zchannel+2*Zpore
    bc_z0 = fnx.DirichletBC(V, fnx.Constant(0.0), 'near(x[1],0)')
    bc_z1 = fnx.DirichletBC(V, fnx.Constant(1.0), 'near(x[1],%f)'%Zmax)
    
    # Define variational problem
    u = fnx.TrialFunction(V)
    v = fnx.TestFunction(V)
    r = fnx.Expression('x[0]',degree=1) # For Cylindrical Jacobian need a factor of r
    f = fnx.Constant(0.0)
    a = fnx.dot(fnx.grad(u), fnx.grad(v))*r*fnx.dx
    L = f*v*r*fnx.dx
    
    # Compute solution
    u = fnx.Function(V)
    fnx.solve(a == L, u, [bc_z0,bc_z1])
    fnx.plot(u,rasterized=True)
    fnx.plot(u,mode='contour',colors='k',linestyles='--',
             levels=[0.0,0.2,0.4,0.45,0.47,0.49,0.5,0.51,0.53,0.55,0.6,0.8,1.0],
             rasterized=True)
    plt.gca().get_xaxis().set_ticks([])
    plt.gca().get_yaxis().set_ticks([])
    plt.gcf().set_size_inches(3,3*(Zchannel+2*Zpore)/Rchannel)
    plt.tight_layout()
    plt.savefig(QAdir+"/soln_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.eps"%
                (Rpore,Zpore,Rchannel,Zchannel,res_mesh,Nr,Nz),
                dpi=QAdpi)
    plt.close()
    
    # Compute error
    comp_rCylLap_full = r*fnx.div(fnx.grad(u)) + fnx.grad(u)[0]
    rCylLap_full = fnx.project(comp_rCylLap_full,V)
    fnx.plot(rCylLap_full,mode='contour',levels=[0.0],colors='k')
    plt.gca().get_xaxis().set_ticks([])
    plt.gca().get_yaxis().set_ticks([])
    plt.gcf().set_size_inches(3+1,3*(Zchannel+2*Zpore)/Rchannel)
    plt.tight_layout()
    plt.savefig(QAdir+"/rCylLap_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.eps"%
                (Rpore,Zpore,Rchannel,Zchannel,res_mesh,Nr,Nz),
                dpi=QAdpi)
    fnx.plot(mesh)
    plt.xlim([0,2*FineZone*Rpore])
    plt.ylim([0,2*FineZone*Zpore])
    plt.savefig(QAdir+"/rCylLap_zoom_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.eps"%
                (Rpore,Zpore,Rchannel,Zchannel,res_mesh,Nr,Nz),
                dpi=QAdpi)
    plt.close()
    rCylLap_vals = rCylLap_full.compute_vertex_values()
    print("Field error summary:")
    print("\tMax rCylLap=%.2e"%rCylLap_vals.max())
    print("\tMin rCylLap=%.2e"%rCylLap_vals.min())
    print("\tMean rCylLap=%.2e"%rCylLap_vals.mean())
    print("\tStd rCylLap=%.2e"%rCylLap_vals.std())
    
    # Plot of field along r=0
    z = mesh.coordinates()[:,1]
    Fz = fnx.project(fnx.grad(u)[1],V).compute_vertex_values()
    plt.semilogy(z,Fz,'.')
    Fz_pore = np.mean(Fz[np.where(z<Zpore)])
    plt.axhline(Fz_pore,color='k')
    plt.axhline(Fz_pore*(Rpore/Rchannel)**2,color='k')
    plt.xlabel("z")
    plt.ylabel("Fz")
    plt.savefig(QAdir+"/Fz_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.eps"%
                (Rpore,Zpore,Rchannel,Zchannel,res_mesh,Nr,Nz),
                dpi=QAdpi)
    plt.close()
    
    # Interpolate onto rectangular mesh
    # Higher resolution probably slows down simulation
    mesh2 = fnx.RectangleMesh(pos_chnlBL,pos_chnlUR,Nr,Nz)
    V2 = fnx.FunctionSpace(mesh2,'P',2)
    u2 = fnx.interpolate(u,V2)

    # Compute error
    comp_rCylLap_full = r*fnx.div(fnx.grad(u2)) + fnx.grad(u2)[0]
    rCylLap_full = fnx.project(comp_rCylLap_full,V2)
    fnx.plot(rCylLap_full)
    plt.savefig(QAdir+"/projected_rCylLap_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.eps"%
                (Rpore,Zpore,Rchannel,Zchannel,res_mesh,Nr,Nz),
                dpi=QAdpi)
    fnx.plot(mesh2)
    plt.xlim([0,2*FineZone*Rpore])
    plt.ylim([0,2*FineZone*Zpore])
    plt.savefig(QAdir+"/projected_rCylLap_zoom_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.eps"%
                (Rpore,Zpore,Rchannel,Zchannel,res_mesh,Nr,Nz),
                dpi=QAdpi)
    plt.close()
    rCylLap_vals = rCylLap_full.compute_vertex_values()
    print("Projected field error summary:")
    print("\tMax rCylLap=%.2e"%rCylLap_vals.max())
    print("\tMin rCylLap=%.2e"%rCylLap_vals.min())
    print("\tMean rCylLap=%.2e"%rCylLap_vals.mean())
    print("\tStd rCylLap=%.2e"%rCylLap_vals.std())

    # Plot of field along r=0
    z = mesh2.coordinates()[:,1]
    Fz = fnx.project(fnx.grad(u2)[1],V2).compute_vertex_values()
    plt.semilogy(z,Fz,'.')
    plt.axhline(Fz_pore,color='k')
    plt.axhline(Fz_pore*(Rpore/Rchannel)**2,color='k')
    plt.xlabel("z")
    plt.ylabel("Fz")
    plt.savefig(QAdir+"/projected_Fz_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.eps"%
                (Rpore,Zpore,Rchannel,Zchannel,res_mesh,Nr,Nz),
                dpi=QAdpi)
    plt.close()

    # Save as numpy array
    u2_vals = u2.compute_vertex_values()
    u2_vals = u2_vals.reshape([u2_vals.size,1])
    mesh2_vals = mesh2.coordinates()
    r = mesh2_vals[:,0]
    z = mesh2_vals[:,1] - Zpore - Zchannel/2. # Re-center to middle of channel
    r = r.reshape((-1,1))
    z = z.reshape((-1,1))
    U = u2_vals
    output_array = np.hstack([r,z,U])
    np.savetxt(fielddir+"/U_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.npy"%
              (Rpore,Zpore,Rchannel,Zchannel,res_mesh,Nr,Nz),
              output_array)
