import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.colors as colors
import sys
import glob
import scipy.stats as sps


datadir = 'data/'
# LCs = [10,25,50,75,100,150,200]
LCs = [25,50,75,100,150,200]
# Rchannels = [5.,25.,30.,40.,50.,60.,70.,300.]
# Zchannels = [50.,100.,150.,200.,300.,400.,600.]
# Rchannels = [30,45,60,90,150]
# Zchannels = [30,60,90,150,300]
Rchannels = [30,45,60,75,90,150]
Zchannels = [30,60,90,150,300]
# LCs = [10,50]
# Rchannels = [5.]
# Zchannels = [50.]
Npart=10240
rseed=1
# timestep_cutoff=int(2e8)
# OutFreq=int(1e6)
timestep_cutoffs=[int(2e6),int(2e7),int(4e7),int(2e8)]
OutFreqs=[int(1e4),int(1e5),int(2e5),int(1e6)]




### 2017/05/03: Tabulate mean and std of trns as functions of time for each geometry
for Rchannel in Rchannels:
    for Zchannel in Zchannels:
        for timestep_cutoff in timestep_cutoffs:
            for OutFreq in OutFreqs:
                times = range(1+timestep_cutoff/OutFreq)
                index = pd.MultiIndex.from_product([LCs,times], names=['LC','time'])
                params = pd.DataFrame(index=index,
                                      columns=['mean','std'])
                
                try:
                    for LC in LCs:
                        df = pd.read_csv(datadir+"/trns_%d_%.1f_%.1f_%d_%d_%d_%d.csv" %
                                         (LC,Rchannel,Zchannel,Npart,
                                          rseed,timestep_cutoff,OutFreq)
                                         ,index_col=0)
                        params.loc[LC,'mean'] = df.mean(axis=1).values
                        params.loc[LC,'std'] = df.std(axis=1).values
                    params.to_csv("statistics/meantrns_%.1f_%.1f_%d_%d_%d_%d.csv"%
                                  (Rchannel,Zchannel,Npart,
                                   rseed,timestep_cutoff,OutFreq))
                except:
                    print "Couldn't find case Rch = %.1f, Zch = %.1f, LC = %d."%(Rchannel,Zchannel,LC)
        
