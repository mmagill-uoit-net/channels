import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.stats as sps
import sys
import matplotlib.ticker as mtick



### Main function at the end of file


###########################################################################################

def AnalyseMeanTrns(df):

    params = pd.DataFrame(columns=['LC','Rchannel','Zchannel','mobility'])
    i=0
    for Rchannel in Rchannels:
        for Zchannel in Zchannels:
            # Load mean and std
            df = pd.read_csv("statistics/meantrns_%.1f_%.1f_%d_%d_%d_%d.csv"%
                             (Rchannel,Zchannel,Npart,
                              rseed,timestep_cutoff,OutFreq),index_col=0)
            # Turn all indices into columns
            df = df.reset_index()
            
            # Rescale time
            df.time = df.time * OutFreq * dt
    
            if (df.isnull().values.any() == False):
                fig, ax = plt.subplots()
    
                # Plot mean with std as error bars for all LCs
                for LC in LCs[:]:
                    dfLC = df[df.LC==LC]
                    dfLC.plot(ax=ax,
                              x='time',y='mean',yerr='std',
                              linestyle='-',marker='.',
                              label='LC = %d'%LC)
    
                    # Line of best fit. Come back to error analysis later.
                    myfit, residuals, rank, singular_values, rcond = np.polyfit(
                        x=dfLC.time, y=dfLC['mean'], deg=1, full=True)
                    fitline = np.poly1d(myfit)
                    plt.plot(dfLC.time,fitline(dfLC.time),'k--',zorder=10)
                    params.loc[i] = [LC,Rchannel,Zchannel,myfit[0]]
                    i += 1
    
                plt.legend()
                plt.ylabel("Channel Number")
                plt.xlabel("Time, Simulation Units")
                plt.title("Rchannel = %.1f, Zchannel = %.1f"%(Rchannel,Zchannel))
                # plt.show()
                plt.savefig("plots/meantrns_%.1f_%.1f_%d_%d_%d_%d.png"%
                             (Rchannel,Zchannel,Npart,
                              rseed,timestep_cutoff,OutFreq),
                            dpi=400)
                plt.close()
    params.to_csv("mobilities/mobilities.csv")



#######################################################################################

# LCs = [10,25,50,75,100,150,200]
LCs = [25,50,75,100,150,200]
# Rchannels = [25.,30.,40.,50.,60.,70.,300.]
# Zchannels = [50.,100.,150.,200.,300.,400.,600.]
Rchannels = [30,45,60,75,90,150]
Zchannels = [30,60,75,90,150,300]
Npart=10240
rseed=1
# timestep_cutoff=int(2e8)
# OutFreq=int(1e6)
timestep_cutoffs=[int(2e6),int(2e7),int(4e7),int(2e8)]
OutFreqs=[int(1e4),int(1e5),int(2e5),int(1e6)]
dt = 0.01

# Rchannels = [30]
# Zchannels = [30]
# timestep_cutoffs=[int(2e8)]
# OutFreqs = [int(1e6)]

mpl.rcParams['lines.linewidth'] = 2
mpl.rcParams['font.size'] = 18

# params = pd.DataFrame(columns=['LC','Rchannel','Zchannel','mobility'])
# i=0
# for Rchannel in Rchannels:
#     for Zchannel in Zchannels:
#         for timestep_cutoff in timestep_cutoffs:
#             for OutFreq in OutFreqs:
#                 try:
#                     # Load mean and std
#                     df = pd.read_csv("statistics/meantrns_%.1f_%.1f_%d_%d_%d_%d.csv"%
#                                      (Rchannel,Zchannel,Npart,
#                                       rseed,timestep_cutoff,OutFreq),index_col=0)
#                     # Turn all indices into columns
#                     df = df.reset_index()
                    
#                     # Rescale time
#                     df.time = df.time * OutFreq * dt
            
#                     if (df.isnull().values.any() == False):
#                         fig, ax = plt.subplots(figsize=(11,8))
            
#                         # Plot mean with std as error bars for all LCs
#                         for LC in LCs[:]:
#                             dfLC = df[df.LC==LC]
#                             dfLC.plot(ax=ax,
#                                       x='time',y='mean',yerr='std',
#                                       linestyle='-',marker='.',
#                                       label='%d'%LC)
#                                       # label='N = %d'%LC)
            
#                             # Line of best fit. Come back to error analysis later.
#                             myfit, residuals, rank, singular_values, rcond = np.polyfit(
#                                 x=dfLC.time, y=dfLC['mean'], deg=1, full=True)
#                             fitline = np.poly1d(myfit)
#                             plt.plot(dfLC.time,fitline(dfLC.time),'k--',zorder=10)
#                             # params.loc[i] = [LC,Rchannel,Zchannel,myfit[0]]
#                             params.loc[i] = [LC,Rchannel,Zchannel,myfit[0]*Zchannel] # Normalize by Zch
#                             i += 1
            
#                         l = plt.legend(title='N',ncol=4,borderpad=0.2,handletextpad=0.2,columnspacing=0.4,handlelength=0.5)
#                         l.set_zorder(20)
#                         plt.ylabel("Channel Number")
#                         plt.xlabel("Time")
#                         ax.xaxis.set_ticks([0,timestep_cutoff/200,timestep_cutoff/100])
#                         # ax.xaxis.set_ticklabels(['0','1e6','2e6'])
#                         plt.ylim(ymin=0)
#                         # plt.ylim([0,550])
#                         # ax.xaxis.set_major_formatter(mtick.FormatStrFormatter('%.0g'))
#                         # plt.title("Rchannel = %.1f, Zchannel = %.1f"%(Rchannel,Zchannel))
#                         plt.title("R$_{\mathrm{channel}}$ = %d, Z$_\mathrm{channel}$ = %d"%(Rchannel,Zchannel))
#                         plt.tight_layout()
#                         # plt.subplots_adjust(left=0.20, right=0.93, top=0.92, bottom=0.14)
#                         # plt.show()
#                         plt.savefig("plots/meantrns_%.1f_%.1f_%d_%d_%d_%d.png"%
#                                      (Rchannel,Zchannel,Npart,
#                                       rseed,timestep_cutoff,OutFreq),
#                                     dpi=400)
#                         plt.close()
#                     else:
#                         print "This"
#                 except:
#                     print "Something failed"
# params.to_csv("mobilities/mobilities.csv")
# # sys.exit()

mpl.rcParams['lines.linewidth'] = 3
mpl.rcParams['font.size'] = 18


# Not capable of handling different timestep_cutoff and OutFreq values together yet... come back to this
df_mob = pd.read_csv("mobilities/mobilities.csv",index_col=0)
df_mob = df_mob[df_mob.Zchannel>30]
df_mob = df_mob.groupby(['LC','Rchannel','Zchannel']).mean().reset_index().sort_values(by=['LC','Rchannel','Zchannel'])
#df_mob.mobility *= df_mob.Zchannel # Normalize by number of nanopores
# Average over repeated timestep_cutoff or OutFreq


for Rchannel in Rchannels:
    fig, ax = plt.subplots(figsize=(10,7))
    dfR_mob = df_mob[df_mob.Rchannel==Rchannel]
    # dfR_mob = dfR_mob.sort_values('LC')
    for Zchannel in dfR_mob.Zchannel.unique():
        dfRZ_mob = dfR_mob[dfR_mob.Zchannel==Zchannel]
        dfRZ_mob.plot(ax=ax,x='LC',y='mobility',label='Z$_{\mathrm{channel}}$ = %d'%Zchannel)
    #plt.show()
    plt.legend(title='R$_{\mathrm{channel}}$ = %d'%Rchannel)
    plt.xlabel("Chain Length")
    plt.ylabel("Mobility [Distance/Time]")
    plt.ylim(ymin=0)
    # plt.ylim(ymax=0.59)
    # plt.title("Rchannel = %d"%Rchannel)
    # plt.yscale('log', nonposy='clip')
    # plt.ylim([5e-3,2e-1])
    plt.tight_layout()
    # plt.subplots_adjust(left=0.16, right=0.93, top=0.98, bottom=0.16)
    # plt.savefig("plots/mobR_%d_%d_%d_%d_%d.png"%
    #             (Rchannel,Npart,
    #              rseed,timestep_cutoff,OutFreq),
    #             dpi=400)
    plt.savefig("plots/mobR_%d_%d_%d.png"%
                (Rchannel,Npart,
                 rseed),
                dpi=400)
    plt.close()


for Zchannel in Zchannels:
    fig, ax = plt.subplots(figsize=(10,7))
    dfZ_mob = df_mob[df_mob.Zchannel==Zchannel]
    dfZ_mob = dfZ_mob.sort_values('LC')
    for Rchannel in dfZ_mob.Rchannel.unique():
        dfZR_mob = dfZ_mob[dfZ_mob.Rchannel==Rchannel]
        dfZR_mob.plot(ax=ax,x='LC',y='mobility',label='R$_{\mathrm{channel}}$ = %d'%Rchannel)
    #plt.show()
    plt.legend(title='Z$_{\mathrm{channel}}$ = %d'%Zchannel)
    plt.xlabel("Chain Length")
    plt.ylabel("Mobility [Distance/Time]")
    plt.ylim(ymin=0)
    plt.ylim(ymax=0.59)
    # plt.title("Zchannel = %d"%Zchannel)
    # plt.yscale('log', nonposy='clip')
    plt.subplots_adjust(left=0.16, right=0.93, top=0.98, bottom=0.16)
    # plt.tight_layout()
    # plt.savefig("plots/mobZ_%d_%d_%d_%d_%d.png"%
    #             (Zchannel,Npart,
    #              rseed,timestep_cutoff,OutFreq),
    #             dpi=400)
    plt.savefig("plots/mobZ_%d_%d_%d.png"%
                (Zchannel,Npart,
                 rseed),
                dpi=400)
    plt.close()
sys.exit()

# for Rchannel in Rchannels:
#     fig, ax = plt.subplots()
#     dfR_mob = df_mob[df_mob.Rchannel==Rchannel]
#     for Zchannel in dfR_mob.Zchannel.unique():
#         dfRZ_mob = dfR_mob[dfR_mob.Zchannel==Zchannel]
#         mobderiv = np.diff(dfRZ_mob.mobility)/np.diff(dfRZ_mob.LC)
#         #dfRZ_mob.plot(ax=ax,x='LC',y='mobility',label='Zch = %d'%Zchannel)
#         ax.plot(dfRZ_mob['LC'].iloc[1:],mobderiv,label='Zch = %d'%Zchannel)
#     plt.axhline(0,color='k')
#     plt.legend()
#     plt.xlabel("Chain Length")
#     plt.ylabel("Derivative of Mobility")
#     plt.title("Rchannel = %d"%Rchannel)
#     plt.tight_layout()
#     # plt.show()
#     # sys.exit()
#     plt.savefig("plots/mobderivR_%d_%d_%d_%d_%d.png"%
#                 (Rchannel,Npart,
#                  rseed,timestep_cutoff,OutFreq),
#                 dpi=400)
#     plt.close()


# for Zchannel in Zchannels:
#     fig, ax = plt.subplots()
#     dfZ_mob = df_mob[df_mob.Zchannel==Zchannel]
#     for Rchannel in dfZ_mob.Rchannel.unique():
#         dfZR_mob = dfZ_mob[dfZ_mob.Rchannel==Rchannel]
#         mobderiv = np.diff(dfZR_mob.mobility)/np.diff(dfZR_mob.LC)
#         #dfZR_mob.plot(ax=ax,x='LC',y='mobility',label='Rch = %d'%Rchannel)
#         ax.plot(dfZR_mob['LC'].iloc[1:],mobderiv,label='Rch = %d'%Rchannel)
#     plt.axhline(0,color='k')
#     plt.legend()
#     plt.xlabel("Chain Length")
#     plt.ylabel("Mobility")
#     plt.title("Zchannel = %d"%Zchannel)
#     plt.tight_layout()
#     # plt.show()
#     # sys.exit()
#     plt.savefig("plots/mobderivZ_%d_%d_%d_%d_%d.png"%
#                 (Zchannel,Npart,
#                  rseed,timestep_cutoff,OutFreq),
#                 dpi=400)
#     plt.close()


