from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import pandas as pd
import scipy as sp
import scipy.interpolate
import scipy.stats
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import Wedge
import matplotlib.colors as colors
import time
import sys
from numba import cuda
import math

maxuint64=np.iinfo(np.uint64).max

## CLI ##
LC       = float(sys.argv[1])
Rchannel = float(sys.argv[2])
Zchannel = float(sys.argv[3])

## Notes ##
# Coordinates are centered at the middle of the channels
print("Notes\n---")
print("Copied from meso. Going to trim down to simple Monte Carlo with loaded time parameters.")
print("IG fits aren't perfect, but I'll use those for now.")
print("---")

ttotal = time.time()
######### Initialization #########
# Seed random number generator
rseed=1
np.random.seed(rseed)

# Numerical Parameters
Npart = 10240
print("Running with %d particles."%Npart)
dt = np.float32(0.01)
timestep_cutoff = int(4e7)

# Flags
PlotFlag=0
QAFlag=1
OutFlag=1
OutFreq=int(2e5)

# Initialize arrays
trns = np.zeros(Npart,dtype=np.int32)
timer = np.zeros(Npart,dtype=np.int32)-1 # Initialize at -1 to get CUDA code to assign random times

######### End Initialization #########


########## Load Micro Translocation Times ##########
tmicro_params = np.loadtxt("../micro/igfit/igfit.dat")
eval_tmicro = sp.interpolate.interp1d(tmicro_params[:,0],[tmicro_params[:,1],tmicro_params[:,3]])
tmicro_pymu, tmicro_pylambda = eval_tmicro(LC)
tmicro_mymu = tmicro_pymu * tmicro_pylambda
tmicro_mylambda = tmicro_pylambda
print("Micro time parameters loaded.")
print("\ttmicro_mu = %f"%tmicro_mymu)
print("\ttmicro_lambda = %f"%tmicro_mylambda)
########## End Load Translocation Times ##########


########## Load Meso Translocation Times ##########
#tmeso_params = pd.read_csv(
#    "../meso/archive/oldplots/2017-05-02/rcap2-combined/tmeso/tmeso_IGfits.csv",index_col=0)
tmeso_params = pd.read_csv(
    "../meso/plots/tmeso_IGfits.csv",index_col=0)
# Use naive params
#eval_tmeso = sp.interpolate.interp1d(tmeso_params.LC,[tmeso_params.pymu,tmeso_params.pylambda]) 
tmeso_mymu, tmeso_mylambda = tmeso_params[(tmeso_params.LC==LC)&
                                          (tmeso_params.Rchannel==Rchannel)&
                                          (tmeso_params.Zchannel==Zchannel)][['mymu','mylambda']].iloc[0]
# Meso parameters are in timesteps. Express in time units.
tmeso_mymu = tmeso_mymu * dt
tmeso_mylambda = tmeso_mylambda * dt
print("Meso time parameters loaded.")
print("\ttmeso_mu = %f"%tmeso_mymu)
print("\ttmeso_lambda = %f"%tmeso_mylambda)
########## End Load Translocation Times ##########


########## Initialize Output ##########
suffix = "%d_%.1f_%.1f_%d_%d_%d_%d"%(LC,Rchannel,Zchannel,Npart,rseed,timestep_cutoff,OutFreq)

# The trns vector at each output time
df_trns = pd.DataFrame()
ftrnsname = ("data/trns_%s.csv"%suffix)
df_trns = df_trns.append(pd.DataFrame(index=np.arange(1+timestep_cutoff/OutFreq),columns=np.arange(Npart).T))
df_trns.iloc[0] = trns.T
df_trns.to_csv(ftrnsname)

########## End Initialize Output ##########


########## Functions ##########

## http://tinyurl.com/kotjjo8
@cuda.jit(device=True)
def cuda_xorshiftstar(states, id):
    x = states[id]
    x ^= x >> 12
    x ^= x << 25
    x ^= x >> 27
    states[id] = x
    return np.uint64(x) * np.uint64(2685821657736338717)


@cuda.jit()
def CUDA_ApplyField(trns,timer,states,randnums):
    # i is particle index, which I think corresponds to a given thread
    i = cuda.grid(1)
    # If ever indexing particles that don't exist, ignore them
    if i >= Npart:
        return

    # Main loop
    for timestep in range(OutFreq):

        # Increment timers
        if timer[i]>0:
            timer[i] -= 1

        # Cross channels
        if timer[i]<1:
            # Increment channel unless it is the first timestep
            if timer[i]==0:
                trns[i] += 1

            # Draw tmicro (Wikipedia)
            randnums[i] = cuda_xorshiftstar(states,i)
            U1 = float(randnums[i])/float(maxuint64)
            randnums[i] = cuda_xorshiftstar(states,i)
            U2 = float(randnums[i])/float(maxuint64)
            nu = math.sqrt(-2.*math.log(U1))*math.cos(2*math.pi*U2)
            y = nu**2.
            x = (tmicro_mymu + 
                 tmicro_mymu**2.*y/(2.*tmicro_mylambda) -
                 ( (tmicro_mymu/(2.*tmicro_mylambda))*
                   math.sqrt(4.*tmicro_mymu*tmicro_mylambda*y + 
                             tmicro_mymu**2.*y**2.) ) )
            randnums[i] = cuda_xorshiftstar(states,i)
            z = float(randnums[i])/float(maxuint64)
            if z <= (tmicro_mymu/(tmicro_mymu+x)):
                tmicro_draw = int(math.ceil(x))
            else:
                tmicro_draw = int(math.ceil(tmicro_mymu**2./x))

            # Draw tmeso (Wikipedia)
            randnums[i] = cuda_xorshiftstar(states,i)
            U1 = float(randnums[i])/float(maxuint64)
            randnums[i] = cuda_xorshiftstar(states,i)
            U2 = float(randnums[i])/float(maxuint64)
            nu = math.sqrt(-2.*math.log(U1))*math.cos(2*math.pi*U2)
            y = nu**2.
            x = (tmeso_mymu + 
                 tmeso_mymu**2.*y/(2.*tmeso_mylambda) -
                 ( (tmeso_mymu/(2.*tmeso_mylambda))*
                   math.sqrt(4.*tmeso_mymu*tmeso_mylambda*y + 
                             tmeso_mymu**2.*y**2.) ) )
            randnums[i] = cuda_xorshiftstar(states,i)
            z = float(randnums[i])/float(maxuint64)
            if z <= (tmeso_mymu/(tmeso_mymu+x)):
                tmeso_draw = int(math.ceil(x))
            else:
                tmeso_draw = int(math.ceil(tmeso_mymu**2./x))

            # Reset timer
            timer[i] = tmicro_draw + tmeso_draw



########## End Functions ##########


########## Main Loop ##########

# Initial plotting
if PlotFlag==1:
       mymin = trns.min()
       mymax = trns.max()
       myptp = mymax-mymin
       nbins = 30 # Max nbins (approx; excludes buffer bins)
       bs = np.ceil(float(myptp+1)/float(nbins))
       plt.hist(trns,bins=np.arange(mymin-bs,(mymax+bs)+bs+1,bs)-0.5,normed=True)
       plt.show()

# NVIDIA 1080 has max tpb of 1024, but wiggle room needed
tpb = 1024/2
bpg = int(np.ceil(Npart/1024.))*1024/tpb

# Initialize RNG
states = np.arange(tpb*bpg, dtype=np.uint64)+1 # 1 state per thread
randnums = 0*states

# Send to CUDA in groups of OutFreq timesteps
for timestep_group in range(timestep_cutoff/OutFreq):

    # Main calculation in CUDA
    tloop = time.time()
    CUDA_ApplyField[bpg,tpb](trns,timer,states,randnums)
    timestep = (timestep_group+1)*OutFreq
    dtloop = time.time()-tloop
    if QAFlag==1:
        print("%d timesteps completed:\n"%timestep +
              "\tLast %d timesteps took %.2fs\n"%(OutFreq,dtloop))

    # Output
    if OutFlag==1:
        # Trns
        df_trns.iloc[timestep_group+1] = trns.T
        

    # Plotting
    if PlotFlag==1:
        mymin = trns.min()
        mymax = trns.max()
        myptp = mymax-mymin
        nbins = 30 # Max nbins (approx; excludes buffer bins)
        bs = np.ceil(float(myptp+1)/float(nbins))
        plt.hist(trns,bins=np.arange(mymin-bs,(mymax+bs)+bs+1,bs)-0.5,normed=True)
        plt.show()


df_trns.to_csv(ftrnsname)
print("Main loop finished.")
########## End Main Loop ##########

dttotal = time.time() - ttotal
print("\nTotal Runtime: %.2e s"%dttotal)




##############################################################################################
##############################################################################################
##############################################################################################
########################## DEPRECATED CODE ###################################################
##############################################################################################
##############################################################################################
##############################################################################################


