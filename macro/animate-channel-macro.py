import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.colors as colors
import sys
import glob
import scipy.stats as sps


datadir = 'data/'
# datadir = 'archive/2017-05-04/2e6-1e4/data'
LCs = [25,50,75,100,150,200]
# Rchannels = [25.,30.,40.,50.,60.,70.,300.]
# Zchannels = [50.,100.,150.,200.,300.,400.,600.]
# Rchannels = [150.,300.]
# Zchannels = [700.,1000.]
# Rchannels = [30,45,60,75,90,150]
# Zchannels = [30,45,60,75,90,100,150,200,300]
# Rchannels = [30,45,60,75,90,150]
# Zchannels = [30,60,90,150,300]
Rchannels = [45]
Zchannels = [150]
Npart=10240
rseed=1
timestep_cutoffs=[int(2e6),int(2e7),int(4e7),int(2e8)]
OutFreqs=[int(1e4),int(1e5),int(2e5),int(1e6)]
# timestep_cutoff=int(2e6)
# OutFreq=int(1e4)

# mpl.rcParams['font.size']=18


# ### 2017/05/04: Plot a single frame
# def update_hist(num,DfDict):
#     plt.cla()
#     for LC in LCs:
#         df = DfDict[LC]
#         mymin = df.iloc[num].min().min()
#         mymax = df.iloc[num].max().max()
#         myptp = mymax-mymin
#         bs = np.ceil(float(myptp+1)/float(nbins))
#         #print "%d %d %d %f"%(LC,mymin,mymax,bs)
#         #bs = 1
#         df.iloc[num].hist(bins=np.arange(mymin-bs,(mymax+bs)+bs+1,bs)-0.5,
#                           label='N=%d'%LC,normed=True,
#                           alpha=0.8,fill=True,
#                           histtype='step',edgecolor='k')
#     # plt.legend(loc='center right', bbox_to_anchor=(1.3, 0.5))
#     if num == 50:
#         plt.legend(loc='upper right')
#     plt.ylim([0,globymax*1.25])
#     plt.xlim([0-0.5,globmax+0.5])
#     #plt.yscale('log', nonposy='clip')
#     plt.grid(False)
#     # plt.title("Rchannel = %.1f, Zchannel = %.1f"%(Rchannel,Zchannel))
#     plt.xlabel("Channel Number")
#     plt.ylabel("Probability")
#     print "\t\tFinished frame %d"%num

# nbins = 10
# for Rchannel in Rchannels:
#     for Zchannel in Zchannels:
#         for timestep_cutoff in timestep_cutoffs:
#             for OutFreq in OutFreqs:
#                 print "Starting Rch = %.1f, Zch = %.1f"%(Rchannel,Zchannel)
#                 try:
#                     # Load all dfs into a dict
#                     DfDict = { LC : pd.read_csv(datadir+"/trns_%d_%.1f_%.1f_%d_%d_%d_%d.csv"%
#                                            (LC,Rchannel,Zchannel,Npart,
#                                             rseed,timestep_cutoff,OutFreq),index_col=0) 
#                           for LC in LCs}
                
#                     # Find maximum channel number in this simulation
#                     globmax = -1
#                     for LC in LCs:
#                         df = DfDict[LC]
#                         df_globmax = df.max().max()
#                         if df_globmax > globmax:
#                             globmax = df_globmax
#                     globbs = np.ceil(float(globmax+1)/float(nbins))
                
#                     # Figure out the right ymax to use
#                     globymax = -1
#                     samplebin = 100
#                     for LC in LCs:
#                         df = DfDict[LC]
#                         mymin = df.iloc[samplebin].min().min()
#                         mymax = df.iloc[samplebin].max().max()
#                         myptp = mymax-mymin
#                         bs = np.ceil(float(myptp+1)/float(nbins))
#                         curymax = np.max(np.histogram(df.iloc[samplebin],bins=np.arange(mymin-bs,(mymax+bs)+bs+1,bs)-0.5,normed=True)[0])
#                         if curymax > globymax:
#                             globymax = curymax
                
#                     # Animation
#                     for t_snap in [50, 100, 200]:
#                         fig = plt.figure(figsize=(5.4,4.5))
#                         update_hist(t_snap,DfDict)
#                         # box = plt.gca().get_position()
#                         # plt.gca().set_position([box.x0, box.y0, box.width * 0.85, box.height])
#                         #plt.show()
#                         plt.subplots_adjust(left=0.23, right=0.9975, top=1.0, bottom=0.16)
#                         plt.savefig("plots/trns_%.1f_%.1f_%d_%d_%d_%d__%d.png"%
#                                     (Rchannel,Zchannel,Npart,rseed,timestep_cutoff,OutFreq,t_snap))
#                         plt.close()
#                         print "\tDone."
#                 except:
#                     print "\tCase not found."


### 2017/05/04: Animated histograms with linear scales
def update_hist(num,DfDict):
    plt.cla()
    for LC in LCs:
        df = DfDict[LC]
        mymin = df.iloc[num].min().min()
        mymax = df.iloc[num].max().max()
        myptp = mymax-mymin
        bs = np.ceil(float(myptp+1)/float(nbins))
        #print "%d %d %d %f"%(LC,mymin,mymax,bs)
        #bs = 1
        df.iloc[num].hist(bins=np.arange(mymin-bs,(mymax+bs)+bs+1,bs)-0.5,
                          label='N=%d'%LC,normed=True,
                          alpha=0.8,fill=True,
                          histtype='step',edgecolor='k')
    plt.legend(loc='center right', bbox_to_anchor=(1.3, 0.5))
    plt.ylim([0,globymax*1.25])
    plt.xlim([0-0.5,globmax+0.5])
    #plt.yscale('log', nonposy='clip')
    plt.grid(False)
    plt.title("Rchannel = %.1f, Zchannel = %.1f"%(Rchannel,Zchannel))
    plt.xlabel("Channel Number")
    plt.ylabel("Probability")
    print "\t\tFinished frame %d"%num

nbins = 10
#for Rchannel in Rchannels:
#    for Zchannel in Zchannels:
for Rchannel in Rchannels:
    for Zchannel in Zchannels:
        for timestep_cutoff in timestep_cutoffs:
            for OutFreq in OutFreqs:
                print "Starting Rch = %.1f, Zch = %.1f"%(Rchannel,Zchannel)
                try:
                    # Load all dfs into a dict
                    DfDict = { LC : pd.read_csv(datadir+"/trns_%d_%.1f_%.1f_%d_%d_%d_%d.csv"%
                                           (LC,Rchannel,Zchannel,Npart,
                                            rseed,timestep_cutoff,OutFreq),index_col=0) 
                          for LC in LCs}
        
                    # Find maximum channel number in this simulation
                    globmax = -1
                    for LC in LCs:
                        df = DfDict[LC]
                        df_globmax = df.max().max()
                        if df_globmax > globmax:
                            globmax = df_globmax
                    globbs = np.ceil(float(globmax+1)/float(nbins))
        
                    # Figure out the right ymax to use
                    globymax = -1
                    samplebin = 100
                    for LC in LCs:
                        df = DfDict[LC]
                        mymin = df.iloc[samplebin].min().min()
                        mymax = df.iloc[samplebin].max().max()
                        myptp = mymax-mymin
                        bs = np.ceil(float(myptp+1)/float(nbins))
                        curymax = np.max(np.histogram(df.iloc[samplebin],bins=np.arange(mymin-bs,(mymax+bs)+bs+1,bs)-0.5,normed=True)[0])
                        if curymax > globymax:
                            globymax = curymax
        
                    # Animation
                    fig = plt.figure()
                    update_hist(0,DfDict)
                    box = plt.gca().get_position()
                    plt.gca().set_position([box.x0, box.y0, box.width * 0.85, box.height])
                    num_frames = timestep_cutoff/OutFreq
                    print "\tCreating animation..."
                    line_ani = animation.FuncAnimation(fig, update_hist, num_frames, fargs=(DfDict, ) )
                    # plt.show()
                    # sys.exit()
                    Writer = animation.writers['avconv']
                    writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
                    print "\tSaving animation..."
                    line_ani.save("movies/trns_lin_%.1f_%.1f_%d_%d_%d_%d.mp4"%
                                    (Rchannel,Zchannel,Npart,rseed,timestep_cutoff,OutFreq),
                                  writer=writer)
                    plt.close(fig)
                    print "\tDone."
                except:
                    print "\tCase not found."


# ### 2017/05/02: Animated histograms -- copied from meso
# def update_hist(num,DfDict):
#     plt.cla()
#     for LC in LCs:
#         df = DfDict[LC]
#         mymin = df.iloc[num].min().min()
#         mymax = df.iloc[num].max().max()
#         myptp = mymax-mymin
#         bs = np.ceil(float(myptp+1)/float(nbins))
#         #print "%d %d %d %f"%(LC,mymin,mymax,bs)
#         #bs = 1
#         df.iloc[num].hist(bins=np.arange(mymin-bs,(mymax+bs)+bs+1,bs)-0.5,
#                           label='LC=%d'%LC,normed=True,
#                           alpha=0.8,fill=True,
#                           histtype='step',edgecolor='k')
#     plt.legend(loc='center right', bbox_to_anchor=(1.3, 0.5))
#     plt.ylim([0.5/(globbs*Npart),1])
#     plt.xlim([0-0.5,globmax+0.5])
#     plt.yscale('log', nonposy='clip')
#     plt.grid(False)
#     plt.title("Rchannel = %.1f, Zchannel = %.1f"%(Rchannel,Zchannel))
#     plt.xlabel("Channel Number")
#     plt.ylabel("Probability")
#     print "\t\tFinished frame %d"%num

# nbins = 10
# for Rchannel in Rchannels:
#     for Zchannel in Zchannels:
#         print "Starting Rch = %.1f, Zch = %.1f"%(Rchannel,Zchannel)
#         try:
#             # Load all dfs into a dict
#             DfDict = { LC : pd.read_csv(datadir+"/trns_%d_%.1f_%.1f_%d_%d_%d_%d.csv"%
#                                    (LC,Rchannel,Zchannel,Npart,
#                                     rseed,timestep_cutoff,OutFreq),index_col=0) 
#                   for LC in LCs}

#             # Find maximum channel number in this simulation
#             globmax = -1
#             for LC in LCs:
#                 df = DfDict[LC]
#                 df_globmax = df.max().max()
#                 if df_globmax > globmax:
#                     globmax = df_globmax
#             globbs = np.ceil(float(globmax+1)/float(nbins))

#             # Animation
#             fig = plt.figure()
#             update_hist(0,DfDict)
#             box = plt.gca().get_position()
#             plt.gca().set_position([box.x0, box.y0, box.width * 0.85, box.height])
#             num_frames = timestep_cutoff/OutFreq
#             print "\tCreating animation..."
#             line_ani = animation.FuncAnimation(fig, update_hist, num_frames, fargs=(DfDict, ) )
#             # plt.show()
#             # sys.exit()
#             Writer = animation.writers['avconv']
#             writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
#             print "\tSaving animation..."
#             line_ani.save("movies/trns_log_%.1f_%.1f_%d_%d_%d_%d.mp4"%
#                             (Rchannel,Zchannel,Npart,rseed,timestep_cutoff,OutFreq),
#                           writer=writer)
#             plt.close(fig)
#             print "\tDone."
#         except:
#             print "\tCase not found."
