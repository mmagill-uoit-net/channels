#!/bin/bash
# MOAB/Torque submission script for multiple, dynamically-run 
# serial jobs on SciNet GPC
#
#PBS -l nodes=5:ppn=8,walltime=5:00
#PBS -N serialdynamicMulti



# Parameters
dVexp=0.2			# 200 mV
reff=0.8 			# Largest forbidding hairpins without crashing
leff=1.0			# Thinnest membrane possible
muflag=1			# NaCl
nevents=10			# 
visflag=0			# 
rseed_suffix=123456


# DIRECTORY TO RUN - $PBS_O_WORKDIR is directory job was submitted from
cd $PBS_O_WORKDIR
 
# Turn off implicit threading in Python, R
export OMP_NUM_THREADS=1
 
module load gcc/4.8.1 openmpi/gcc/1.6.4 fftw/3.3.4-gcc-openmpi gnu-parallel/20140622
 
# START PARALLEL JOBS USING NODE LIST IN $PBS_NODEFILE
for N in 10
do
    for i in `seq 1 64`
    do
	rseed=${N}${i}${rseed_suffix}
	echo "/home/h/hdh/magill/espresso-channels-micro/Espresso channels-micro-scinet.tcl $N $dVexp $reff $leff $muflag $nevents $rseed $visflag"
    done
done | parallel -j8 --sshloginfile $PBS_NODEFILE --workdir $PWD


## # Test
## #PBS -l nodes=1:ppn=8,walltime=1:00:00
## #PBS -q debug
## module load gcc/4.8.1 openmpi/gcc/1.6.4 fftw/3.3.4-gcc-openmpi
## for rseed_prefix in {0..7}
## do
##     N=10
##     rseed=${N}${rseed_prefix}${rseed_suffix}
##     /home/h/hdh/magill/espresso-channels-micro/Espresso /scratch/h/hdh/magill/channels/micro/channels-micro-scinet.tcl $N $dVexp $reff $leff $muflag $nevents $rseed $visflag &
## done
## wait
