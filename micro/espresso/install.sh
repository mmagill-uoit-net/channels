#!/bin/bash


# Casename
casename="channels-micro"

# Unzip espresso and move to appropriate folder name
CURDIR=`pwd`
cd ~
tar -zxf Espresso-2.1.2j.tar.gz
mv espresso-2.1.2j espresso-${casename}
cd $CURDIR

# Move files over
cp forces.c      ~/espresso-${casename}/.
cp myconfig.h    ~/espresso-${casename}/.
cp auxiliary.tcl ~/espresso-${casename}/scripts/.

# Compile
cd ~/espresso-${casename}
mkdir obj-Opteron-pc-linux
cd obj-Opteron-pc-linux
../configure --without-mpi myconfig=myconfig.h --build=Opteron-pc-linux
make
cd ..
mkdir obj-Xeon_64-pc-linux
cd obj-Xeon_64-pc-linux
../configure --without-mpi myconfig=myconfig.h --build=Xeon_64-pc-linux
make
cd ..
cd $CURDIR
