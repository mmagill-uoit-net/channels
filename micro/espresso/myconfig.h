/* global features */
#define PARTIAL_PERIODIC
#define ELECTROSTATICS
#define EXTERNAL_FORCES
#define CONSTRAINTS
#define MASS
#define EXCLUSIONS
#define COMFORCE
#define COMFIXED
#define ROTATION
#define NPT

/* potentials */
#define TABULATED
#define LENNARD_JONES
#define LENNARD_JONES_GENERIC
#define MORSE
#define LJCOS
#define LJCOS2
#define BUCKINGHAM
#define SOFT_SPHERE
#define BOND_ANGLE_HARMONIC
#define FORCE_CORE
