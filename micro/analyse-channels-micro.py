import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.stats as sps

mpl.rcParams['font.size']=18
# mpl.rcParams['figure.figsize']=(8,6)
mpl.rcParams['lines.linewidth']=3


###
print("Notes")
print("---")
print("Took out the (rcap,rcut) stuff because the new format is a fraction. Not sure what the best approach is. Title currently commented.")
print("---")
###


# Load the data
df = pd.read_csv("gathered-channels-micro.csv",index_col=0)

# # Remove N=10
# df = df[df.N>10]

try:
    params = np.loadtxt('igfit/igfit.dat')
except:
    pass








###########################################################################################


def GroupPlot(df):

    fig, axarr = plt.subplots(3,1,dpi=700,figsize=(8,17))

    df.sort_values(by='N',inplace=True)
    Ns = df.N.unique()*1.0

    #
    df_means = df.groupby('N').t.mean()
    df_vars = df.groupby('N').t.var()
    df_counts = df.groupby('N').t.count()
    
    # Plot mean with power law fit
    plt.sca(axarr[0])
    plt.plot(Ns,df_means,'o-',ms=12,label='')
    m,b = np.polyfit(np.log(Ns),np.log(df_means),1)
    plt.plot(Ns,np.exp(b)*Ns**m,'k--',
             label='$%.2fN^{%.2f}$'%(np.exp(b),m))
    plt.xscale('log')
    plt.yscale('log')
    plt.legend()
    plt.xlabel('Chain Length, $N$')
    plt.ylabel('Mean $t_\mathrm{micro}$')
    plt.text(150,100,'(a)',fontsize=20)
    
    # Plot mean with power law fit
    plt.sca(axarr[1])
    plt.plot(Ns,df_vars,'o-',ms=12,label='')
    m,b = np.polyfit(np.log(Ns),np.log(df_vars),1)
    plt.plot(Ns,np.exp(b)*Ns**m,'k--',
             label='$%.2fN^{%.2f}$'%(np.exp(b),m))
    plt.xscale('log')
    plt.yscale('log')
    plt.legend()
    plt.xlabel('Chain Length, $N$')
    plt.ylabel('Variance of $t_\mathrm{micro}$')
    plt.text(150,4e4,'(b)',fontsize=20)

    # Violin plot
    plt.sca(axarr[2])
    parts = plt.violinplot([grp.t for key,grp in df.groupby('N')],
                           positions=Ns,widths=10,showextrema=False)
    for pc in parts['bodies']:
        pc.set_facecolor('#49beffff')
    plt.plot(Ns,[grp.t.mean() for key,grp in df.groupby('N')],
             'x',ms=10,mew=3)
    plt.yscale('log')
    plt.xlabel('Chain Length, $N$')
    plt.ylabel('Distribution of $t_\mathrm{micro}$')
    plt.text(180,14,'(c)',fontsize=20)

    # Save
    plt.tight_layout()
    plt.savefig('plots/micro-results.eps',
                format='eps',dpi=700)
    plt.close()





def FailRates(df):
    # Fraction of events retracted, timeout'ed, and total failed
    pretract = df.groupby('N')['nretract'].agg({'p' : (lambda x : float(x.sum())/float(x.sum()+x.count())),'num':'count'}).reset_index()
    ptimeout = df.groupby('N')['ntimeout'].agg({'p' : (lambda x : float(x.sum())/float(x.sum()+x.count())),'num':'count'}).reset_index()
    df['nfail'] = df['nretract'] + df['ntimeout']
    df['pfail'] = df['nfail']/(df['nfail']+1)
    pfail = df.groupby('N')['nfail'].agg({'p' : (lambda x : float(x.sum())/float(x.sum()+x.count())),'num':'count'}).reset_index()
    # Plot with CLT error bars
    # plt.errorbar(pretract.N,pretract.p,marker='.',label='Retract',yerr=2.0*np.sqrt(pretract.p*(1-pretract.p)/pretract.num))
    # plt.errorbar(ptimeout.N,ptimeout.p,marker='.',label='Timeout',yerr=2.0*np.sqrt(ptimeout.p*(1-ptimeout.p)/ptimeout.num))
    plt.errorbar(pfail.N,pfail.p,
                 color='k',linestyle='--',marker='.',
                 label='Total',yerr=2.0*np.sqrt(pfail.p*(1-pfail.p)/pfail.num))
    plt.plot(pfail.N,1./pfail.num,'k-',label='1 / Sample Size')
    plt.xlim([0,pfail.N.max()+pfail.N.min()])
    num_max = pfail.num.max()
    plt.ylim([0.5/num_max,1.0])
    plt.yscale('log')
    # plt.text(10,0.1,'Error bars show 2 S.E. assuming CLT.\nSomewhat underestimates error for\nvery small probabilities.')
    plt.text(125,0.08,'Error bars show 2 S.E.\nif the CLT applies.\nThis underestimates\nerror for very small\nprobabilities.',fontsize=16)
    plt.text(20,0.7e-3,'1 / Sample Size',fontsize=16)
    plt.ylabel('Fail Rate')
    plt.xlabel('N')
    # plt.legend()
    plt.tight_layout()
    plt.savefig('plots/micro-failrates.eps',dpi=300)
    plt.close()

def BasicAnalysis(df):
    df.sort_values(by='N',inplace=True)
    Ns = df.N.unique()*1.0

    # Violin plot
    plt.violinplot([grp.t for key,grp in df.groupby('N')],
                   positions=Ns,widths=10,showextrema=False)
    plt.plot(Ns,[grp.t.mean() for key,grp in df.groupby('N')],
             'kx')
    plt.yscale('log')
    plt.xlabel('N')
    plt.ylabel('Microscopic Time')
    plt.tight_layout()
    plt.savefig('plots/micro-violin.eps',dpi=300)
    plt.close()

    #
    df_means = df.groupby('N').t.mean()
    df_vars = df.groupby('N').t.var()
    df_counts = df.groupby('N').t.count()
    
    # # Plot mean with power law fit
    # plt.errorbar(Ns,df_means,yerr=(df_vars/df_counts)**0.5,label='')
    # alpha = 1.7
    # fitN = 75
    # plt.plot(Ns,df_means.loc[fitN]*(Ns/float(fitN))**alpha,'k--',
    #          label='$%.2fN^{%.2f}$'%(df_means.loc[fitN]*(1.0/float(fitN))**alpha,alpha))
    # plt.xscale('log')
    # plt.yscale('log')
    # plt.legend()
    # plt.xlabel('N')
    # plt.ylabel('Mean Microscopic Time w 1 S.E.')
    # plt.tight_layout()
    # plt.show()

    # Plot mean with power law fit
    # plt.errorbar(Ns,df_means,yerr=(df_vars/df_counts)**0.5,label='')
    plt.plot(Ns,df_means,'o-',ms=12,label='')
    m,b = np.polyfit(np.log(Ns),np.log(df_means),1)
    plt.plot(Ns,np.exp(b)*Ns**m,'k--',
             label='$%.2fN^{%.2f}$'%(np.exp(b),m))
    plt.xscale('log')
    plt.yscale('log')
    plt.legend()
    plt.xlabel('N')
    plt.ylabel('Mean Microscopic Time')
    plt.tight_layout()
    plt.savefig('plots/micro-mean.eps',dpi=300)
    plt.close()

    # # Plot variance with power law fit
    # plt.plot(Ns,df_vars,label='')
    # alpha = 3.8
    # fitN = 75
    # plt.plot(Ns,df_vars.loc[fitN]*(Ns/float(fitN))**alpha,'k--',
    #          label='$%.2fN^{%.2f}$'%(df_vars.loc[fitN]*(1.0/float(fitN))**alpha,alpha))
    # plt.xscale('log')
    # plt.yscale('log')
    # plt.legend()
    # plt.xlabel('N')
    # plt.ylabel('Var of Microscopic Time w 1 S.E.')
    # plt.tight_layout()
    # plt.show()

    # Plot mean with power law fit
    plt.plot(Ns,df_vars,'o-',ms=12,label='')
    m,b = np.polyfit(np.log(Ns),np.log(df_vars),1)
    plt.plot(Ns,np.exp(b)*Ns**m,'k--',
             label='$%.2fN^{%.2f}$'%(np.exp(b),m))
    plt.xscale('log')
    plt.yscale('log')
    plt.legend()
    plt.xlabel('N')
    plt.ylabel('Variance of Microscopic Time')
    plt.tight_layout()
    plt.savefig('plots/micro-var.eps',dpi=300)
    plt.close()

    
def IGFits(df):
    mpl.rcParams['axes.formatter.limits'] = [-7, 3]

    # Loop over chain lengths
    Ns = df['N'].unique()
    params_all = np.zeros((Ns.shape[0],4))
    for i,(key,grp) in enumerate(df.groupby('N')):
        N = int(key)
        fig, ax = plt.subplots()
        # Histogram of data
        mymin = grp['t'].min()
        mymax = grp['t'].max()
        myptp = mymax-mymin
        nbins = 100 # Max nbins (approx; excludes buffer bins)
        bs = np.ceil(float(myptp+1)/float(nbins))
        grp.hist('t',ax=ax,bins=np.arange(mymin-bs,(mymax+bs)+bs+1,bs)-0.5,normed=True)
        # Fit
        mymu = grp['t'].mean() # mu of real IG satisfies this
        mylambda =  mymu**3. / grp['t'].var() # lambda of real IG satisfies this
        pymu = mymu/mylambda # Python mu satisfies this
        pylambda = mylambda # Python lambda is the same
        params = sps.invgauss.fit(grp['t'],pymu,floc=0,scale=pylambda)
        params_all[i,:] = [N, params[0], 0, params[2]]
        x = np.linspace(grp['t'].min(),
                        grp['t'].max(), 500)
        ax.plot(x,sps.invgauss.pdf(x,mu=params[0],loc=0,scale=params[2]),label='MLE')
        ax.plot(x,sps.invgauss.pdf(x,mu=mymu/mylambda,loc=0,scale=mylambda),label='Biased Estimator')
        # Clean up plot
        plt.legend()
        ax.set_title("N = %d"%N)
        ax.set_yscale('log')
        plt.ylim(ymin=0.5/(bs*grp.t.count()))
        plt.xlabel("Time, Simulation Units")
        plt.ylabel("Probability")
        plt.tight_layout()
        fig.savefig("igfit/igfit_%d.eps"%N)
        plt.close(fig)

    # Save params_all
    params_all = params_all[params_all[:,0].argsort()]
    np.savetxt("igfit/igfit.dat",params_all)
    return params_all


def IGAnalyse(params):

    # Fit Parameters
    alpha_lambda = 1.75
    alpha_mymu = 1.25
    alpha_v = -alpha_mymu
    alpha_D = -alpha_lambda

    # Plot pymu_fit(N)
    plt.plot(params[:,0],params[:,1])
    slope, intercept, r_value, p_value, std_err = sps.linregress(params[:,0],params[:,1])
    plt.plot(params[:,0],slope*params[:,0]+intercept,
             label='slope = %.2e, intecept = %.2f'%(slope,intercept) )
    plt.xlabel('N')
    plt.ylabel(r'Fitted IG $\mu_\mathrm{Py}$')
    #plt.title('Rcap ~ %d, Rcut ~ %d'%(rcap,rcut))
    plt.legend()
    plt.show()

    # Plot pylambda_fit(N) log-log
    plt.loglog(params[:,0],params[:,3])
    plt.loglog(params[:,0],(params[0,3]*
                              (params[:,0]/params[0,0])**alpha_lambda),
               label='alpha=%.2f'%alpha_lambda)
    plt.xlabel('N')
    plt.ylabel(r'Fitted IG $\lambda_\mathrm{Py}$')
    #plt.title('Rcap ~ %d, Rcut ~ %d'%(rcap,rcut))
    plt.legend()
    plt.tight_layout()
    plt.show()

    # Recall: mymu = L/v, mylambda = L^2/2D
    myN = params[:,0]
    mymu_fit = params[:,1]*params[:,3]
    mylambda_fit = params[:,3]
    v_over_L = 1./mymu_fit
    twoD_over_L2 = 1./mylambda_fit
    twoD_over_v2 = twoD_over_L2 / v_over_L**2.
    peclet = 2.0 * mylambda_fit / mymu_fit
    myvar = mymu_fit**3./mylambda_fit
    mystd = myvar**0.5
    
    # Plot mymu_fit
    plt.figure(figsize=(7.5,5.5),dpi=300)
    plt.loglog(myN, mymu_fit)
    plt.loglog(params[:,0],(mymu_fit[0]*
                            (params[:,0]/params[0,0])**alpha_mymu),'k--',lw=2,
               label='alpha=%.2f'%alpha_mymu)
    plt.xlabel('Chain Length',fontsize=18)
    plt.ylabel(r'Fitted IG $\mu$')
    # plt.ylabel(r'Mean $t_\mathrm{micro}$')
    # #plt.title('Rcap ~ %d, Rcut ~ %d'%(rcap,rcut))
    plt.legend()
    plt.tight_layout()
    plt.savefig("fig.eps")
    plt.close()
    
    # Plot v_over_L
    plt.loglog(myN, v_over_L)
    plt.loglog(myN, v_over_L[0]*(myN/myN[0])**alpha_v,
               label='alpha = %.2f'%alpha_v)
    plt.xlabel('N')
    plt.ylabel(r'Fitted $v/L$')
    #plt.title('Rcap ~ %d, Rcut ~ %d'%(rcap,rcut))
    plt.legend()
    plt.show()

    # Plot twoD_over_L2
    plt.plot(myN, twoD_over_L2)
    plt.loglog(myN, twoD_over_L2[0]*(myN/myN[0])**alpha_D,
               label='alpha = %.2f'%alpha_D)
    plt.xlabel('N')
    plt.ylabel(r'Fitted $2D/L^2$')
    #plt.title('Rcap ~ %d, Rcut ~ %d'%(rcap,rcut))
    plt.legend()
    plt.show()

    # Plot twoD_over_v2: IGs add to IGs iff this quantity matches
    plt.plot(myN,twoD_over_v2)
    plt.xlabel('N')
    plt.ylabel(r'Fitted $2D/v^2$')
    #plt.title('Rcap ~ %d, Rcut ~ %d'%(rcap,rcut))
    plt.show()

    # Plot Peclet
    plt.loglog(myN,peclet)
    plt.loglog(myN,myN**0.588 * 1.75 / 100**0.588,label='$N^{0.588}$')
    plt.legend()
    plt.xlabel('N')
    plt.ylabel(r'Fitted $vL/D = 2\lambda/\mu$')
    #plt.title('Rcap ~ %d, Rcut ~ %d'%(rcap,rcut))
    plt.tight_layout()
    plt.savefig('igfit/peclet.eps',dpi=300)
    plt.close()

    # Assuming that L~N^nu, correct v and D
    my_vest = v_over_L * myN**0.588
    my_Dest = twoD_over_L2 * (myN**0.588)**2.0 * 0.5

    # Plot vest
    plt.loglog(myN,my_vest)
    plt.loglog(myN,my_vest[0]*(myN/myN[0])**(-0.588),label='$N^{-0.588}$')
    plt.legend()
    plt.xlabel('N')
    plt.ylabel(r'Fitted v/L times $N^{0.588}$')
    #plt.title('Rcap ~ %d, Rcut ~ %d'%(rcap,rcut))
    plt.tight_layout()
    plt.savefig('igfit/vest.eps',dpi=300)
    plt.close()

    # Plot Dest
    plt.loglog(myN,my_Dest)
    plt.loglog(myN,my_Dest[0]*(myN/myN[0])**(-0.588),label='$N^{-0.588}$')
    plt.legend()
    plt.xlabel('N')
    plt.ylabel(r'Fitted 2D/L^2 times $0.5N^{2(0.588)}$')
    #plt.title('Rcap ~ %d, Rcut ~ %d'%(rcap,rcut))
    plt.tight_layout()
    plt.savefig('igfit/Dest.eps',dpi=300)
    plt.close()

    # RePlot twoD_over_v2 with fit
    plt.loglog(myN,twoD_over_v2)
    plt.loglog(myN,twoD_over_v2[0]*(myN/myN[0])**(0.588),label='$N^{0.588}$')
    plt.legend()
    plt.xlabel('N')
    plt.ylabel(r'Fitted $2D/v^2$')
    #plt.title('Rcap ~ %d, Rcut ~ %d'%(rcap,rcut))
    plt.tight_layout()
    plt.savefig('igfit/twoD_over_v2.eps',dpi=300)
    plt.close()
    
    # Plot standard deviation
    plt.plot(myN,mystd)
    plt.xlabel('N')
    plt.ylabel('Theoretical Standard Deviation')
    #plt.title('Rcap ~ %d, Rcut ~ %d'%(rcap,rcut))
    plt.show()

    # Plot coefficient of variation
    plt.plot(myN,mystd/mymu_fit)
    plt.xlabel('N')
    plt.ylabel('Theoretical Coeff of Var')
    #plt.title('Rcap ~ %d, Rcut ~ %d'%(rcap,rcut))
    plt.show()



GroupPlot(df)
