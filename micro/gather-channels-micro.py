import pandas as pd
import glob

pd.concat([pd.read_csv(file,index_col=False)
           for file in glob.glob("data/*.dat")
           if 1<sum(1 for line in open(file))],ignore_index=True).to_csv("gathered-channels-micro.csv")
