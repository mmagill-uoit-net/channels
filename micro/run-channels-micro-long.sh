#!/bin/bash


# Overhead
mkdir -p logs
mkdir -p data
rseed_suffix=123456


# Preliminary study uses these parameters
casename="channels-micro-long"
dVexp=0.2			# 200 mV
reff=0.8 			# Largest forbidding hairpins without crashing
leff=1.0			# Thinnest membrane possible
muflag=1			# NaCl
nevents=10			# 
visflag=0			# 


# Sharcnet parameters
mpp="1g"


# Running/Submission Loop
for rseed_prefix in {00..199}
do
#    for N in 10 20 50 75 100 150 200
    for N in 150 200
    do
	rseed=${N}${rseed_prefix}${rseed_suffix}
#	time=$(echo "$N^2 * 0.1" | bc -l)m
	time=$(echo "$N^2 * 0.2" | bc -l)m
	sqsub --mpp=$mpp -q serial -o logs/${casename}_${muflag}_${dVexp}_${reff}_${leff}_${N}_${rseed}.log -r $time -j "$casename" ~/espresso-${casename}/Espresso ${casename}.tcl $N $dVexp $reff $leff $muflag $nevents $rseed $visflag
#	~/espresso-${casename}/Espresso ${casename}.tcl $N $dVexp $reff $leff $muflag $nevents $rseed $visflag
    done
done
