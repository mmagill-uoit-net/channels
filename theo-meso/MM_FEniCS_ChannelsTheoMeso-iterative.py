from __future__ import print_function
import fenics as fnx
import mshr
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
import math
import os

#fnx.parameters['allow_extrapolation'] = True


#####
print("Notes")
print("---")
print("Seems to work for large N at sufficient resolution, but otherwise get negative answers")
print("---")
#####

# Geometry
Rp=0.8
Zp=0.5
Rch=10.
Zch=10.
res_mesh=100
V0 = 17.312367
# V0 = 0.1
# V0 = 1.0

# FEM
FE_type = 'CG'
FE_degree = 2

####


def SolvePotential():
    ### Solve electric field
    # Create mesh
    pos_por1BL = fnx.Point(0,0)
    pos_por1UR = fnx.Point(Rp,Zp)
    pos_chnlBL = fnx.Point(0,Zp)
    pos_chnlUR = fnx.Point(Rch,Zp+Zch)
    pos_por2BL = fnx.Point(0,Zp+Zch)
    pos_por2UR = fnx.Point(Rp,Zp+Zch+Zp)
    rect1 = mshr.Rectangle(pos_por1BL,pos_por1UR)
    rect2 = mshr.Rectangle(pos_chnlBL,pos_chnlUR)
    rect3 = mshr.Rectangle(pos_por2BL,pos_por2UR)
    rect4 = mshr.Rectangle(fnx.Point(Rp,0),fnx.Point(Rp+0.5,Zp))
    rect5 = mshr.Rectangle(fnx.Point(Rp,Zp+Zch),fnx.Point(Rp+0.5,Zp+Zch+Zp))
    circ1 = mshr.Circle(fnx.Point(Rp+0.5,0),0.5)
    circ2 = mshr.Circle(fnx.Point(Rp+0.5,Zp+Zch+Zp),0.5)
    geo = rect1 + rect2 + rect3 + rect4 - circ1 + rect5 - circ2
    mesh = mshr.generate_mesh(geo,res_mesh) 
   
    # Refine mesh near pores
    FineFactor = 5
    Zmax=Zch+2*Zp
    cell_markers = fnx.CellFunction("bool", mesh)
    cell_markers.set_all(False)
    for cell in fnx.cells(mesh):
        vertices = cell.get_vertex_coordinates().reshape(3,2)
        if ( (vertices[:,1].max()<FineFactor*Zp) and (vertices[:,0].max()<FineFactor*Rp) ):
            cell_markers[cell] = True
        if ( (vertices[:,1].min()>(Zmax-FineFactor*Zp)) and (vertices[:,0].max()<FineFactor*Rp) ):
            cell_markers[cell] = True
    mesh = fnx.refine(mesh, cell_markers)
    
    # Define function space
    V = fnx.FunctionSpace(mesh, FE_type, FE_degree)
    
    # Define boundary conditions (Homogeneous Newmann BCs by default)
    bc_z0 = fnx.DirichletBC(V, fnx.Constant(0.0), 'near(x[1],0)')
    bc_z1 = fnx.DirichletBC(V, fnx.Constant(V0), 'near(x[1],%f)'%Zmax)
    
    # Define variational problem
    u = fnx.TrialFunction(V)
    v = fnx.TestFunction(V)
    r = fnx.Expression('x[0]',degree=1) # For Cylindrical Jacobian need a factor of r
    f = fnx.Constant(0.0)
    a = fnx.dot(fnx.grad(u), fnx.grad(v))*r*fnx.dx
    L = f*v*r*fnx.dx
    A, b = fnx.assemble_system(a,L,[bc_z0,bc_z1])

    # Compute solution
    u = fnx.Function(V)
    fnx.solve(A, u.vector(), b)
    fnx.plot(u)
    plt.savefig('fields/u_%.1f_%.1f_%.1f_%.1f_%.1f_%d.png'%(Rp,Zp,Rch,Zch,V0,res_mesh),dpi=200)
    plt.close()
    
    # Save and return
    field_filename = 'fields/u_%.1f_%.1f_%.1f_%.1f_%.1f_%d.xml'%(Rp,Zp,Rch,Zch,V0,res_mesh)
    fieldmesh_filename = 'fields/umesh_%.1f_%.1f_%.1f_%.1f_%.1f_%d.xml'%(Rp,Zp,Rch,Zch,V0,res_mesh)
    fnx.File(fieldmesh_filename) << mesh
    fnx.File(field_filename) << u
    return u






def PlotG0i(i,N,g0i,mesh):
    fig,ax = plt.subplots()
    plt.tricontourf(mesh.coordinates()[:,0],
                    mesh.coordinates()[:,1],
                    mesh.cells(),
                    g0i.compute_vertex_values(mesh),
                    200)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    # plt.xlim([0,Rch])
    # plt.ylim([Zp,Zp+Zch])
    ax.set_aspect('equal')
    plt.subplots_adjust(top=1.0,bottom=0.0,left=0.0,right=1.0)
    plt.colorbar()
    plt.tight_layout()
    plt.savefig('g0is/g0i_%.1f_%.1f_%.1f_%.1f_%.1f_%d_%d__%d.png'%(Rp,Zp,Rch,Zch,V0,res_mesh,N,i),dpi=200)
    plt.close()

def PlotG0(N,g0,mesh):
    fig,ax = plt.subplots()
    plt.tricontourf(mesh.coordinates()[:,0],
                    mesh.coordinates()[:,1],
                    mesh.cells(),
                    g0.compute_vertex_values(mesh),
                    200)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    # plt.xlim([0,Rch])
    # plt.ylim([Zp,Zp+Zch])
    ax.set_aspect('equal')
    plt.subplots_adjust(top=1.0,bottom=0.0,left=0.0,right=1.0)
    plt.colorbar()
    plt.tight_layout()
    plt.savefig('g0s/g0_%.1f_%.1f_%.1f_%.1f_%.1f_%d_%d.png'%(Rp,Zp,Rch,Zch,V0,res_mesh,N),dpi=200)
    plt.close()

def SaveG0i(i,N,g0i,mesh):
    g0i_filename = 'g0is/g0i_%.1f_%.1f_%.1f_%.1f_%.1f_%d_%d__%d.xml'%(Rp,Zp,Rch,Zch,V0,res_mesh,N,i)
    g0imesh_filename = 'g0is/g0imesh_%.1f_%.1f_%.1f_%.1f_%.1f_%d_%d__%d.xml'%(Rp,Zp,Rch,Zch,V0,res_mesh,N,i)
    fnx.File(g0imesh_filename) << mesh
    fnx.File(g0i_filename) << g0i



    

    
def SolveG0i(i,N,g0m,mesh,V,r,u,bc_abs):
    g0i = fnx.TrialFunction(V)
    w = fnx.TestFunction(V)
    a = (1./fnx.Constant(N))*fnx.dot(fnx.grad(g0i), fnx.grad(w))*r*fnx.dx 
    L = -w*fnx.dot(fnx.grad(g0m),fnx.grad(u))*r*fnx.dx
    # L = -w*fnx.div(g0m*fnx.grad(u))*r*fnx.dx
    # L = w*fnx.div(g0m*fnx.grad(u))*r*fnx.dx
    A, b = fnx.assemble_system(a,L,bc_abs)
    ParticlesIn = fnx.PointSource(V, fnx.Point(0,Zp+1.0), 1.0)
    ParticlesIn.apply(b)
    g0i = fnx.Function(V)
    fnx.solve(A, g0i.vector(), b)
    PlotG0i(i,N,g0i,mesh)
    # SaveG0i(i,N,g0i,mesh)
    return g0i

def SolveG00(N,mesh,V,r,u,bc_abs):
    g00 = fnx.TrialFunction(V)
    w = fnx.TestFunction(V)
    f = fnx.Constant(0.0)
    a = (1./N)*fnx.dot(fnx.grad(g00), fnx.grad(w))*r*fnx.dx
    L = f*w*r*fnx.dx
    A, b = fnx.assemble_system(a,L,bc_abs)
    ParticlesIn = fnx.PointSource(V, fnx.Point(0,Zp+1.0), 1.0)
    ParticlesIn.apply(b)
    g00 = fnx.Function(V)
    fnx.solve(A, g00.vector(), b)
    PlotG0i(0,N,g00,mesh)
    # SaveG0i(0,N,g00,mesh)
    return g00




def SolveG0(N):
    ### Solve CDE
    # Create mesh
    pos_por1BL = fnx.Point(0,0)
    pos_por1UR = fnx.Point(Rp,Zp)
    pos_chnlBL = fnx.Point(0,Zp)
    pos_chnlUR = fnx.Point(Rch,Zp+Zch)
    pos_por2BL = fnx.Point(0,Zp+Zch)
    pos_por2UR = fnx.Point(Rp,Zp+Zch+Zp)
    rect1 = mshr.Rectangle(pos_por1BL,pos_por1UR)
    rect2 = mshr.Rectangle(pos_chnlBL,pos_chnlUR)
    rect3 = mshr.Rectangle(pos_por2BL,pos_por2UR)
    Rcap = N/4.
    circ1 = mshr.Circle(pos_por2BL,Rcap)
    circ2 = mshr.Circle(fnx.Point(0,Zp),1.0)
#    geo = rect1 + rect2 + rect3 - circ1
    geo = rect2 + rect3 - circ1 - circ2
    mesh = mshr.generate_mesh(geo,res_mesh) 
    
    # Refine mesh near pores
    FineFactor = 5
    Zmax=Zch+2*Zp
    cell_markers = fnx.CellFunction("bool", mesh)
    cell_markers.set_all(False)
    for cell in fnx.cells(mesh):
        vertices = cell.get_vertex_coordinates().reshape(3,2)
        if ( (vertices[:,1].max()<FineFactor*Zp) and (vertices[:,0].max()<FineFactor*Rp) ):
            cell_markers[cell] = True
        if ( (vertices[:,1].min()>(Zmax-FineFactor*Zp)) and (vertices[:,0].max()<FineFactor*Rp) ):
            cell_markers[cell] = True
    mesh = fnx.refine(mesh, cell_markers)
    
    # Define function space
    V = fnx.FunctionSpace(mesh, FE_type, FE_degree)
    r = fnx.Expression('x[0]',degree=1)

    # Define boundary conditions (Homogeneous Newmann BCs by default)
    class CapRadius(fnx.SubDomain):
        def inside(self,x,on_boundary):
            return on_boundary and x[0]<Rcap and x[1]>Zch+Zp-Rcap
    boundaries = fnx.FacetFunction("size_t", mesh, 0) 
    CapRadius().mark(boundaries, 1)
    bc_abs = fnx.DirichletBC(V, fnx.Constant(0), boundaries, 1)

    # Load field if it already exists, otherwise compute
    field_filename = 'fields/u_%.1f_%.1f_%.1f_%.1f_%.1f_%d.xml'%(Rp,Zp,Rch,Zch,V0,res_mesh)
    fieldmesh_filename = 'fields/umesh_%.1f_%.1f_%.1f_%.1f_%.1f_%d.xml'%(Rp,Zp,Rch,Zch,V0,res_mesh)
    if os.path.exists(field_filename) and os.path.exists(fieldmesh_filename):
        mesh_old = fnx.Mesh(fieldmesh_filename)
        V_old = fnx.FunctionSpace(mesh_old, FE_type, FE_degree)
        u_raw = fnx.Function(V_old, field_filename)
    else:
        u_raw = SolvePotential()
    # Project field onto mesh
    u = fnx.interpolate(u_raw,V)

    # Loop through g0i -> g0(i+1)
    Lambda = 1.0 #1.0/N
    g00 = SolveG00(N,mesh,V,r,u,bc_abs)
    g0 = g00*1.0
    g0m = g00*1.0
    for i in range(1,500):
        g0n = SolveG0i(i,N,g0m,mesh,V,r,u,bc_abs)
        g0m = fnx.project(Lambda*g0n + (1-Lambda)*g0m,V)
    g0 = fnx.project(g0m,V)
    PlotG0(N,g0,mesh)
    return fnx.assemble(g0*r*fnx.dx), g00


##########################################################################################################
##########################################################################################################
##########################################################################################################
##########################################################################################################
##########################################################################################################


Ns = np.array([10,20,50,75,100,150,200,300])
Ns = np.array([35,20,30])
MFPTs = 0.0*Ns
for i,N in enumerate(Ns):
    MFPTs[i], g00 = SolveG0(N)
plt.plot(Ns,MFPTs,'k--*')
plt.xlabel('N')
plt.ylabel('MFPT')
plt.tight_layout()
plt.savefig('mfpts/mfpt_%.1f_%.1f_%.1f_%.1f_%.1f_%d.png'%(Rp,Zp,Rch,Zch,V0,res_mesh),dpi=200)
plt.close()
