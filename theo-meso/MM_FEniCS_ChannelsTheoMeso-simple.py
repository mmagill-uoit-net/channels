from __future__ import print_function
import fenics as fnx
import mshr
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
import math
import os


#####
print("Notes")
print("---")
print("Seems to work for large N at sufficient resolution, but otherwise get negative answers")
print("---")
#####

# Geometry
Rp=0.8
Zp=0.5
Rch=90.
Zch=90.
res_mesh=200
V0 = 17.312367

# FEM
FE_type = 'CG'
FE_degree = 2

####


def SolvePotential():
    ### Solve electric field
    # Create mesh
    pos_por1BL = fnx.Point(0,0)
    pos_por1UR = fnx.Point(Rp,Zp)
    pos_chnlBL = fnx.Point(0,Zp)
    pos_chnlUR = fnx.Point(Rch,Zp+Zch)
    pos_por2BL = fnx.Point(0,Zp+Zch)
    pos_por2UR = fnx.Point(Rp,Zp+Zch+Zp)
    rect1 = mshr.Rectangle(pos_por1BL,pos_por1UR)
    rect2 = mshr.Rectangle(pos_chnlBL,pos_chnlUR)
    rect3 = mshr.Rectangle(pos_por2BL,pos_por2UR)
    geo = rect1 + rect2 + rect3
    mesh = mshr.generate_mesh(geo,res_mesh) 
   
    # Refine mesh near pores
    FineFactor = 5
    Zmax=Zch+2*Zp
    cell_markers = fnx.CellFunction("bool", mesh)
    cell_markers.set_all(False)
    for cell in fnx.cells(mesh):
        vertices = cell.get_vertex_coordinates().reshape(3,2)
        if ( (vertices[:,1].max()<FineFactor*Zp) and (vertices[:,0].max()<FineFactor*Rp) ):
            cell_markers[cell] = True
        if ( (vertices[:,1].min()>(Zmax-FineFactor*Zp)) and (vertices[:,0].max()<FineFactor*Rp) ):
            cell_markers[cell] = True
    mesh = fnx.refine(mesh, cell_markers)
    
    # Define function space
    V = fnx.FunctionSpace(mesh, FE_type, FE_degree)
    
    # Define boundary conditions (Homogeneous Newmann BCs by default)
    bc_z0 = fnx.DirichletBC(V, fnx.Constant(0.0), 'near(x[1],0)')
    bc_z1 = fnx.DirichletBC(V, fnx.Constant(1.0), 'near(x[1],%f)'%Zmax)
    
    # Define variational problem
    u = fnx.TrialFunction(V)
    v = fnx.TestFunction(V)
    r = fnx.Expression('x[0]',degree=1) # For Cylindrical Jacobian need a factor of r
    f = fnx.Constant(0.0)
    a = fnx.dot(fnx.grad(u), fnx.grad(v))*r*fnx.dx
    L = f*v*r*fnx.dx
    A, b = fnx.assemble_system(a,L,[bc_z0,bc_z1])

    # Compute solution
    u = fnx.Function(V)
    fnx.solve(A, u.vector(), b)
    fnx.plot(u)
    plt.savefig('fields/u_%.1f_%.1f_%.1f_%.1f_%d_%d.png'%(Rp,Zp,Rch,Zch,res_mesh,N),dpi=200)
    plt.close()
    
    # Save and return
    field_filename = 'fields/u_%.1f_%.1f_%.1f_%.1f_%d_%d.xml'%(Rp,Zp,Rch,Zch,res_mesh,N)
    fieldmesh_filename = 'fields/umesh_%.1f_%.1f_%.1f_%.1f_%d_%d.xml'%(Rp,Zp,Rch,Zch,res_mesh,N)
    fnx.File(fieldmesh_filename) << mesh
    fnx.File(field_filename) << u
    return u



def SolveG0(N):
    ### Solve CDE
    # Create mesh
    pos_por1BL = fnx.Point(0,0)
    pos_por1UR = fnx.Point(Rp,Zp)
    pos_chnlBL = fnx.Point(0,Zp)
    pos_chnlUR = fnx.Point(Rch,Zp+Zch)
    pos_por2BL = fnx.Point(0,Zp+Zch)
    pos_por2UR = fnx.Point(Rp,Zp+Zch+Zp)
    # rect1 = mshr.Rectangle(pos_por1BL,pos_por1UR)
    rect2 = mshr.Rectangle(pos_chnlBL,pos_chnlUR)
    rect3 = mshr.Rectangle(pos_por2BL,pos_por2UR)
    Rcap = N/4.
    circ1 = mshr.Circle(pos_por2BL,Rcap)
    # geo = rect1 + rect2 + rect3 - circ1
    geo = rect2 + rect3 - circ1
    mesh = mshr.generate_mesh(geo,res_mesh) 
    
    # Refine mesh near pores
    FineFactor = 5
    Zmax=Zch+2*Zp
    cell_markers = fnx.CellFunction("bool", mesh)
    cell_markers.set_all(False)
    for cell in fnx.cells(mesh):
        vertices = cell.get_vertex_coordinates().reshape(3,2)
        if ( (vertices[:,1].max()<FineFactor*Zp) and (vertices[:,0].max()<FineFactor*Rp) ):
            cell_markers[cell] = True
        if ( (vertices[:,1].min()>(Zmax-FineFactor*Zp)) and (vertices[:,0].max()<FineFactor*Rp) ):
            cell_markers[cell] = True
    mesh = fnx.refine(mesh, cell_markers)
    
    # Define function space
    V = fnx.FunctionSpace(mesh, FE_type, FE_degree)
    
    # Load field if it already exists, otherwise compute
    field_filename = 'fields/u_%.1f_%.1f_%.1f_%.1f_%.1f_%d.xml'%(Rp,Zp,Rch,Zch,V0,res_mesh)
    fieldmesh_filename = 'fields/umesh_%.1f_%.1f_%.1f_%.1f_%.1f_%d.xml'%(Rp,Zp,Rch,Zch,V0,res_mesh)
    if os.path.exists(field_filename) and os.path.exists(fieldmesh_filename):
        mesh_old = fnx.Mesh(fieldmesh_filename)
        V_old = fnx.FunctionSpace(mesh_old, FE_type, FE_degree)
        u_raw = fnx.Function(V_old, field_filename)
    else:
        u_raw = SolvePotential()
    # Project field onto mesh
    u = fnx.interpolate(u_raw,V)
    delta_u = u
    
    # Define boundary conditions (Homogeneous Newmann BCs by default)
    class CapRadius(fnx.SubDomain):
        def inside(self,x,on_boundary):
            return on_boundary and x[0]<Rcap and x[1]>Zch+Zp-Rcap
    boundaries = fnx.FacetFunction("size_t", mesh, 0) 
    CapRadius().mark(boundaries, 1)
    bc_abs = fnx.DirichletBC(V, fnx.Constant(0), boundaries, 1)
    
    # Define variational problem
    g0 = fnx.TrialFunction(V)
    w = fnx.TestFunction(V)
    r = fnx.Expression('x[0]',degree=1) # For Cylindrical Jacobian need a factor of r
    f = fnx.Constant(0.0)
    a = (1./N)*fnx.dot(fnx.grad(g0), fnx.grad(w))*r*fnx.dx + w*fnx.div(g0*fnx.grad(u))*r*fnx.dx
    L = f*w*r*fnx.dx
    A, b = fnx.assemble_system(a,L,bc_abs)
    
    # Add point source
    ParticlesIn = fnx.PointSource(V, pos_chnlBL, 1.0)
    ParticlesIn.apply(b)
    
    # Compute solution
    g0 = fnx.Function(V)
    g0.vector()[:] = (V0 - u.vector()[:]*1.0)/V0
    fnx.solve(A, u.vector(), b)
    # solver.solve(A, g0.vector(), b)
    # solver = fnx.KrylovSolver('gmres', 'ilu')
    # prm = solver.parameters
    # prm.absolute_tolerance = 1E-7
    # prm.relative_tolerance = 1E-4
    # prm.maximum_iterations = 2000
    # prm.nonzero_initial_guess = True
    # prm.monitor_convergence = True
    # solver.solve(A, g0.vector(), b)
    
    # Plot
    fig,ax = plt.subplots()
    plt.tricontourf(mesh.coordinates()[:,0],
                    mesh.coordinates()[:,1],
                    mesh.cells(),
                    g0.compute_vertex_values(mesh),
                    200)
                    # levels=np.linspace(0,15,100))
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    plt.xlim([0,Rch])
    plt.ylim([Zp,Zp+Zch])
    ax.set_aspect('equal')
    plt.subplots_adjust(top=1.0,bottom=0.0,left=0.0,right=1.0)
    plt.colorbar()
    plt.tight_layout()
    plt.savefig('g0s/g0_%.1f_%.1f_%.1f_%.1f_%.1f_%d_%d.png'%(Rp,Zp,Rch,Zch,V0,res_mesh,N),dpi=200)
    plt.close()
    
    # Save and return
    g0_filename = 'g0s/g0_%.1f_%.1f_%.1f_%.1f_%.1f_%d_%d.xml'%(Rp,Zp,Rch,Zch,V0,res_mesh,N)
    g0mesh_filename = 'g0s/g0mesh_%.1f_%.1f_%.1f_%.1f_%.1f_%d_%d.xml'%(Rp,Zp,Rch,Zch,V0,res_mesh,N)
    fnx.File(g0mesh_filename) << mesh
    fnx.File(g0_filename) << g0
    return fnx.assemble(g0*r*fnx.dx)
    


def SolveG1(N):
    ### Solve CDE
    # Create mesh
    pos_por1BL = fnx.Point(0,0)
    pos_por1UR = fnx.Point(Rp,Zp)
    pos_chnlBL = fnx.Point(0,Zp)
    pos_chnlUR = fnx.Point(Rch,Zp+Zch)
    pos_por2BL = fnx.Point(0,Zp+Zch)
    pos_por2UR = fnx.Point(Rp,Zp+Zch+Zp)
    rect1 = mshr.Rectangle(pos_por1BL,pos_por1UR)
    rect2 = mshr.Rectangle(pos_chnlBL,pos_chnlUR)
    rect3 = mshr.Rectangle(pos_por2BL,pos_por2UR)
    Rcap = N/4.
    circ1 = mshr.Circle(pos_por2BL,Rcap)
    geo = rect1 + rect2 + rect3 - circ1
    mesh = mshr.generate_mesh(geo,res_mesh)
    
    # Define function space
    V = fnx.FunctionSpace(mesh, FE_type, FE_degree)
    
    # Get u and g0
    #######
    
    # Define boundary conditions (Homogeneous Newmann BCs by default)
    class CapRadius(fnx.SubDomain):
        def inside(self,x,on_boundary):
            return on_boundary and x[0]<Rcap and x[1]>Zch+Zp-Rcap
    boundaries = fnx.FacetFunction("size_t", mesh, 0) 
    CapRadius().mark(boundaries, 1)
    bc_abs = fnx.DirichletBC(V, fnx.Constant(0), boundaries, 1)
    
    # Define variational problem
    g1 = fnx.TrialFunction(V)
    w = fnx.TestFunction(V)
    r = fnx.Expression('x[0]',degree=1) # For Cylindrical Jacobian need a factor of r
    a = (1./N)*fnx.dot(fnx.grad(g1), fnx.grad(w))*r*fnx.dx + w*fnx.div(g1*fnx.grad(u))*r*fnx.dx
    L = g0*w*r*fnx.dx
    A, b = fnx.assemble_system(a,L,bc_abs)
    
    # Compute solution
    g1 = fnx.Function(V)
    fnx.solve(A, g1.vector(), b)
    fnx.plot(g1)
    plt.show()
    return g1





##########################################################################################################
##########################################################################################################
##########################################################################################################
##########################################################################################################
##########################################################################################################


Ns = np.array([10,20,50,75,100,150,200,300])
MFPTs = 0.0*Ns
for i,N in enumerate(Ns):
    MFPTs[i] = SolveG0(N)
plt.plot(Ns,MFPTs,'k--*')
plt.xlabel('N')
plt.ylabel('MFPT')
plt.tight_layout()
plt.savefig('mfpts/mfpt_%.1f_%.1f_%.1f_%.1f_%.1f_%d.png'%(Rp,Zp,Rch,Zch,V0,res_mesh),dpi=200)
plt.close()
