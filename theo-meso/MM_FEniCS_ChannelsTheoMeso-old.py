from __future__ import print_function
import fenics as fnx
import mshr
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
import math


Rp=0.8
Rch=45.
Zch=90.
res_mesh=300
Nr = 100
Nz = 100
N = 10
Rcap = N/4.

# Create mesh
geo = mshr.Rectangle(fnx.Point(0,0),fnx.Point(Rch,Zch))
mesh = mshr.generate_mesh(geo,res_mesh)

# Define function space
V = fnx.FunctionSpace(mesh, 'P', 2)

# # Define boundary conditions (Homogeneous Newmann BCs by default)
# bc_abs = fnx.DirichletBC(V, fnx.Constant(0.0), 'near(x[1],%f)'%Zch)
# bc_abs = fnx.DirichletBC(V, fnx.Constant(0.0),
#                          'near(x[0]*x[0]+(x[1]-%f)*(x[1]-%f),%f)'%(Zch,Zch,Rcap))
def InPore(x,on_boundary):
    return on_boundary and x[0]<Rp and fnx.near(x[1],0)
def OutPore(x,on_boundary):
    return on_boundary and x[0]<Rp and fnx.near(x[1],Zch)
V0 = 17.312367
Vch = V0*(1 - 1./(np.pi*Rp))
bc_fieldin = fnx.DirichletBC(V, fnx.Constant(Vch/2.0),InPore)
bc_fieldout = fnx.DirichletBC(V, fnx.Constant(-Vch/2.0),OutPore)
bc_partout = fnx.DirichletBC(V, fnx.Constant(0.0),OutPore)

### Solve electric field
# Define variational problem
u = fnx.TrialFunction(V)
v = fnx.TestFunction(V)
r = fnx.Expression('x[0]',degree=1) # For Cylindrical Jacobian need a factor of r
f = fnx.Constant(0.0)
a = fnx.dot(fnx.grad(u), fnx.grad(v))*r*fnx.dx
L = f*v*r*fnx.dx
A, b = fnx.assemble_system(a,L,[bc_fieldin,bc_fieldout])

# # Add point source
# Qeff = 2*17.312367/(np.pi*Rp)
# FieldIn = fnx.PointSource(V, fnx.Point(Rp,0), Qeff)
# FieldIn.apply(b)
# FieldOut = fnx.PointSource(V, fnx.Point(Rp,Zch), -Qeff)
# FieldOut.apply(b)

# Compute solution
u = fnx.Function(V)
fnx.solve(A, u.vector(), b)
fnx.plot(u)
plt.show()

### Solve CDE
# Define variational problem
g0 = fnx.TrialFunction(V)
w = fnx.TestFunction(V)
a = fnx.dot(fnx.grad(g0), fnx.grad(w))*r*fnx.dx - N*w*fnx.div(g0*fnx.grad(u))*r*fnx.dx
L = f*w*r*fnx.dx
A, b = fnx.assemble_system(a,L,bc_partout)

# Add point source
ParticlesIn = fnx.PointSource(V, fnx.Point(0,0), -N)
ParticlesIn.apply(b)

# Compute solution
g0 = fnx.Function(V)
fnx.solve(A, g0.vector(), b)
fnx.plot(g0)
plt.show()
# plt.tricontourf(mesh.coordinates()[:,0],
#                 mesh.coordinates()[:,1],
#                 mesh.cells(),
#                 g0.compute_vertex_values(mesh)/mesh.coordinates()[:,0])
# plt.show()


# # Interpolate onto rectangular mesh
# # Higher resolution probably slows down simulation
# mesh2 = fnx.RectangleMesh(fnx.Point(0,0),fnx.Point(Rch,Zch),Nr,Nz)
# V2 = fnx.FunctionSpace(mesh2,'P',2)
# g02 = fnx.interpolate(g0,V2)
# RR = mesh2.coordinates()[:,0].reshape(Nz+1,-1)
# ZZ = mesh2.coordinates()[:,1].reshape(Nz+1,-1)
# GG = g02.compute_vertex_values(mesh2).reshape(Nz+1,-1)
# plt.contourf(RR,ZZ,GG,20)
# plt.show()
# QQ = GG/RR
# QQ[np.where(np.isnan(QQ))] = 0
# plt.contourf(RR,ZZ,QQ,20)
# plt.show()

# # Compute error
# comp_rCylLap_full = r*fnx.div(fnx.grad(u2)) + fnx.grad(u2)[0]
# rCylLap_full = fnx.project(comp_rCylLap_full,V2)
# fnx.plot(rCylLap_full)
# plt.savefig(QAdir+"/projected_rCylLap_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.png"%
#             (Rp,Zpore,Rch,Zch,res_mesh,Nr,Nz),
#             dpi=QAdpi)
# fnx.plot(mesh2)
# plt.xlim([0,2*FineZone*Rp])
# plt.ylim([0,2*FineZone*Zpore])
# plt.savefig(QAdir+"/projected_rCylLap_zoom_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.png"%
#             (Rp,Zpore,Rch,Zch,res_mesh,Nr,Nz),
#             dpi=QAdpi)
# plt.close()
# rCylLap_vals = rCylLap_full.compute_vertex_values()
# print("Projected field error summary:")
# print("\tMax rCylLap=%.2e"%rCylLap_vals.max())
# print("\tMin rCylLap=%.2e"%rCylLap_vals.min())
# print("\tMean rCylLap=%.2e"%rCylLap_vals.mean())
# print("\tStd rCylLap=%.2e"%rCylLap_vals.std())

# # Plot of field along r=0
# z = mesh2.coordinates()[:,1]
# Fz = fnx.project(fnx.grad(u2)[1],V2).compute_vertex_values()
# plt.semilogy(z,Fz,'.')
# plt.axhline(Fz_pore,color='k')
# plt.axhline(Fz_pore*(Rp/Rch)**2,color='k')
# plt.xlabel("z")
# plt.ylabel("Fz")
# plt.savefig(QAdir+"/projected_Fz_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.png"%
#             (Rp,Zpore,Rch,Zch,res_mesh,Nr,Nz),
#             dpi=QAdpi)
# plt.close()

# # Save as numpy array
# u2_vals = u2.compute_vertex_values()
# u2_vals = u2_vals.reshape([u2_vals.size,1])
# mesh2_vals = mesh2.coordinates()
# r = mesh2_vals[:,0]
# z = mesh2_vals[:,1] - Zpore - Zch/2. # Re-center to middle of channel
# r = r.reshape((-1,1))
# z = z.reshape((-1,1))
# U = u2_vals
# output_array = np.hstack([r,z,U])
# np.savetxt(fielddir+"/U_%.1f_%.1f_%.1f_%.1f_%d_%d_%d.npy"%
#           (Rp,Zpore,Rch,Zch,res_mesh,Nr,Nz),
#           output_array)
